;;;; sort.scm --- Sorting routines -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generobert
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:


;;; Code:

(define-library (beans sort)
  (export sort quicksort quicksort* merge-sort merge-sort*)
  (import (scheme base)
          (beans list-utils))
  (begin

    ;; LST is a list of "item" to sort.
    ;; MOREN-THAN--FUNCTION is a function that accepts 2 arguments, which is an value retruned
    ;; by EXTRACT-FUNCTION applied to an "item" from the list LST and a pivot value. Such argument
    ;; is called as the 1st argument in MOREN-THAN--FUNCTION while the pivot value is called
    ;; as the 2nd argument.
    ;; EXTRACT-FUNCTION is a function to use on each "item" in the list LST.
    ;; Returns a sorted list of items
    (define (quicksort* lst more-than--function extract-function)
       ;; An empty list or a list of 1 element is already sorted
      (if (or (null? lst)
              (null? (cdr lst)))
          lst
          (let loop ((pivot (extract-function (car lst)))
                     (others (cdr lst))
                     (less-than-or-equal--list '())
                     (more-than--list '()))
            (cond
             ((null? others)
              (append (quicksort* more-than--list
                                  more-than--function
                                  extract-function)
                      (list (car lst))
                      (quicksort* less-than-or-equal--list
                                  more-than--function
                                  extract-function)))
             ((more-than--function (extract-function (car others)) pivot)
              (loop pivot
                    (cdr others)
                    less-than-or-equal--list
                    (cons (car others) more-than--list)))
             (else
              (loop pivot
                    (cdr others)
                    (cons (car others) less-than-or-equal--list)
                    more-than--list))))))

    (define (quicksort numbers)
      (quicksort* numbers > (lambda (x) x))) ; identity function

    (define (merge-sort* lst more-than--function extract-function)
      ;; it's faster to use local procedure
      (letrec ((merge
                (lambda (list1 list2 result)
                  (cond
                   ((and (null? list1)
                         (null? list2))
                    result)
                   ((null? list1)
                    (merge list1 (cdr list2) (cons (car list2) result)))
                   ((null? list2)
                    (merge (cdr list1) list2 (cons (car list1) result)))
                   (else
                    (let ((head1 (extract-function (car list1)))
                          (head2 (extract-function (car list2))))
                      (if (more-than--function head2 head1)
                          (merge list1
                                 (cdr list2)
                                 (cons (car list2) result))
                          (merge (cdr list1)
                                 list2
                                 (cons (car list1) result)))))))))
        ;; an empty list or a list with 1 element are already "sorted"
        (if (or (null? lst)
                (null? (cdr lst)))
            lst
            (let ((half (quotient (length lst) 2)))
              (reverse (merge (merge-sort* (take lst half)
                                           more-than--function
                                           extract-function)
                              (merge-sort* (drop lst half)
                                           more-than--function
                                           extract-function)
                              '()))))))

    (define (merge-sort lst)
      (merge-sort* lst > (lambda (x) x)))

    (define (sort . numbers)
      (quicksort numbers))))


;;;; sort.scm ends here.
