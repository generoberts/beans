;;;; matrix.scm --- Matrix math -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This is mostly a corner stone for each other math-ish libraries.
;; It use vector internally but communicates with other libraries via list.

;;; Code:

(define-library (beans math matrix)
  (import (scheme base)
          (scheme case-lambda)
          (beans macro)
          (except kawa.lib.prim_imports string) ; for `double[][]'
          (class org.ejml.simple
                 SimpleMatrix SimpleEVD SimpleSVD))
  (export make-matrix make-identity-matrix
          determinant invert psuedo-invert
          add mult hadamard-product dot kron negative matrix-apply
          row-num col-num size row-ref col-ref matrix-ref
          matrix->list solve matrix-trace transpose
          evd eigen-values eigen-vectors
          svd singular-values svd-u svd-v svd-w)
  (begin

    (define (make-matrix 2d-number-list) ::SimpleMatrix
      (let ((double-array ::double[][] (apply double[][]
                                              (map (lambda (row)
                                                     (apply double[] row))
                                                   2d-number-list))))
        (SimpleMatrix double-array)))

    (define (make-identity-matrix width) ::SimpleMatrix
      (SimpleMatrix:identity width))

    
;;; SimpleMatrix section
    (define (determinant m::SimpleMatrix)
      (m:determinant))

    (define (invert m::SimpleMatrix) ::SimpleMatrix
      (m:invert))

    ;; Moore-Penrose pseudo-inverse of the matrix.
    ;; Normally, a non-square matrix doesn't have an inverse, but it's useful to compute one when, for example,
    ;; solving an equation of polynomial regression.
    (define (psuedo-invert m::SimpleMatrix) ::SimpleMatrix
      (m:pseudoInverse))

    ;; If the 2nd argument is a matrix, add those two matrices elementwise.
    ;; If the 2nd argument is a number, add that number to each elemnt in the matrix M.
    (define add
      (case-lambda
       ((m1::SimpleMatrix m2::SimpleMatrix) ::SimpleMatrix
        (m1:plus m2))
       ((m::SimpleMatrix scalar::double) ::SimpleMatrix
        (m:plus scalar))))

    (define (mult m1::SimpleMatrix m2::SimpleMatrix) ::SimpleMatrix
      (m1:mult m2))

    (define (hadamard-product m1::SimpleMatrix m2::SimpleMatrix) ::SimpleMatrix
      (m1:elementMult m2))

    ;; Dot, a.k.a inner, product of 2 vectors.
    (define (dot v1::SimpleMatrix v2::SimpleMatrix)
      (v1:dot v2))

    ;; Kronecker product
    (define (kron m1::SimpleMatrix m2::SimpleMatrix) ::SimpleMatrix
      (m1:kron m2))

    ;; Reverse the sign of all elements in the matrix M.
    (define (negative m::SimpleMatrix) ::SimpleMatrix
      (m:negative))

    ;; Apply the procedure FUNC to all elements of matrix M and return the new matrix.
    ;; FUNC is a function that accepts 1 argument.
    ;; M is a matrix of `SimpleMatrix' type.
    (define (matrix-apply func m::SimpleMatrix) ::SimpleMatrix
      (let ((m-list (matrix->list m)))
        (make-matrix
         (map (lambda (x-row)
                (map (lambda (x)
                       (func x))
                     x-row))
              m-list))))

    ;; The number of row.
    (define (row-num m::SimpleMatrix)
      (m:numRows))

    ;; The number of column.
    (define (col-num m::SimpleMatrix)
      (m:numCols))

    ;; A pair represent a dimension of matrix
    (define (size m::SimpleMatrix)
      (cons (row-num m) (col-num m)))

    ;; The n-th row.
    (define (row-ref m::SimpleMatrix n) ::SimpleMatrix
      (m:rows n (+ n 1)))

    ;; The n-th column.
    (define (col-ref m::SimpleMatrix n) ::SimpleMatrix
      (m:cols n (+ n 1)))

    ;; The specific element in the matrix.
    (define (matrix-ref m::SimpleMatrix row-index col-index)
      (m:get row-index col-index))

    (define (matrix->list m::SimpleMatrix)
      (let* ((cols (m:numCols))
             (rows (m:numRows))
             (index-cols (collect from 0 to cols))
             (index-rows (collect from 0 to rows)))
        (map (lambda (y)
               (map (lambda (x)
                      (m:get y x))
                    index-cols))
             index-rows)))

    ;; Solve the equation aX = b.
    (define (solve a::SimpleMatrix b::SimpleMatrix) ::SimpleMatrix
      (a:solve b))

    (define (matrix-trace m::SimpleMatrix)
      (m:trace))

    (define (transpose m::SimpleMatrix) ::SimpleMatrix
      (m:transpose))

    
;;; SimpleEVD section
    (define (evd m::SimpleMatrix) ::SimpleEVD
      (m:eig))

    ;; Return a list of eigenvalues in no particular order.
    (define (eigen-values m::SimpleEVD)
      (map (lambda (x) x)
           (m:getEigenvalues)))

    ;; Return a list of SimpleMatrix of eigenvectors.
    (define (eigen-vectors m::SimpleEVD)
      (map (lambda (x)
             (m:getEigenVector x))
           (collect from 0 to (m:getNumberOfEigenvalues))))

    
;;; SimpleSVD section
    ;; SVD is of the form M = UWV^T
    (define (svd m::SimpleMatrix) ::SimpleSVD
      (m:svd))

    ;; This is the W matrix, but since it's a diagonal matrix we can use a list to store it like this.
    ;; The list is ordered from largest to smallest singular values.
    (define (singular-values m::SimpleSVD)
      (map (lambda (x) x) ; we have to map over the double[] arrray to change it to a list.
           (m:getSingularValues)))

    (define (svd-u m::SimpleSVD) ::SimpleMatrix
      (m:getU))

    (define (svd-v m::SimpleSVD) ::SimpleMatrix
      (m:getV))

    (define (svd-w m::SimpleSVD) ::SimpleMatrix
      (m:getW))
    ))


;;;; matrix.scm ends here.
