;;;; arc4random.scm --- RC4 random number generator -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; An implementation of RC4 cipher, for use as a random number generator.
;; RC4 consists of Key-scheduling algorithm (KSA) and Pseudo-random generation algorithm (PRGA).
;; The pseudocode for them are given below.

;; The reason for this library is that, the library `(beans random)` relies *too much* on Apacahe's
;; Common RNG, a library of JVM. Thus, it isn't portable. While we can use linear congruent generator,
;; LCG for short, to generate random numbers, its statistical properties isn't *that* good. RC4, though
;; it's a broken algorithm and has some bias, has much better statistical properties.
;; However, RC4 is slower than LCG, sometimes it's even 10 times slower. Consider these trade off when
;; you choose a random number generator for your use case.

;; NOTE: This library generates 64 bits random numbers.
;; NOTE: This library is seedable, to set a seed, call `(set-random-seed! you-seed)' where `your-seed'
;; is a number you want to use as a seed. After you set a new seed, you should also discard the first
;; few bytes by calling (prga) several times (like a thousand times or more).

;; Psudo code for RC4:
;; KSA:
;; for i from 0 to 255
;;     S[i] := i
;; endfor
;; j := 0
;; for i from 0 to 255
;;     j := (j + S[i] + key[i mod keylength]) mod 256
;;     swap values of S[i] and S[j]
;; endfor
;;
;; PRGA:
;; i := 0
;; j := 0
;; while GeneratingOutput:
;;     i := (i + 1) mod 256
;;     j := (j + S[i]) mod 256
;;     swap values of S[i] and S[j]
;;     K := S[(S[i] + S[j]) mod 256]
;;     output K
;; endwhile

;; Code:

(define-library (beans random arc4random)
  (import (scheme base)
          (scheme time)
          (scheme inexact)
          (beans macro)
          (beans list-utils)
          (only (beans math) +PI+))
  (export set-random-seed! random-integer random random-gaussian random-pick random-shuffle
          random-real random-true-false random-n-sphere)
  (begin

    ;; A simple "random" number generator to use to generate the seed of `arc4random'.
    (define word-size (- (expt 2 64) 59))

    (define %lcg
      (let ((a 18263440312458789471)
            (m word-size)
            (seed (current-jiffy)))
        (lambda new-seed
          (if (= (length new-seed) 1)
              (set! seed (car new-seed))
              (set! seed (modulo (* seed a) m)))
          (modulo seed m))))

    ;; Normally, the first few outputs of a random number generator aren't that good, so we have to
    ;; "exercise" it a bit.
    ;; We call it 10000 times, which is arbitrary, and hope that the quality will be better.
    (repeat 10000
      (%lcg))

    (define (%random-int-in-range n)
      (let ((mod (modulo word-size n)))
        (call/cc
         (lambda (k)
           (do ((i 0 0))
               (#false)
             (let ((num-rand (%lcg)))
               (when (<= num-rand (- word-size mod))
                 (k (modulo num-rand n)))))))))

    (define (%lcg-integer . n)
      (if (null? n)
          (%lcg)
          (%random-int-in-range (car n))))

    ;; We are making a 64 bits at a time using `random-integer'.
    (define +interger-size+ (expt 2 64))

    (define +S+ (apply bytevector (collect from 0 to 256)))

    ;; Swap the `i'th byte and `j'th byte in bytevector `bv'.
    (define (bytevector-swap! bv i j)
      (let ((tmp (bytevector-u8-ref bv i)))
        (bytevector-u8-set! bv i (bytevector-u8-ref bv j))
        (bytevector-u8-set! bv j tmp)))

    ;; Sum two number, `m' and `n', and take the result mod 256.
    (define (+mod256 m n)
      (modulo (+ m n) 256))

    ;; Turns a key string into a bytevector.
    (define key->bytevector string->utf8)

    (define (KSA s-array key)
      (let ((result s-array)
            (j 0)
            (key-length (bytevector-length key)))
        (do ((i 0 (+ i 1)))
            ((= i 256) result)
          (set! j (modulo (+ j (bytevector-u8-ref result i) (bytevector-u8-ref key (modulo i key-length))) 256))
          (bytevector-swap! result i j))))

    ;; `current-jiffy' is an implementation-depended number, so we can't count on that to produce a large
    ;; number of bits for the key. So we mulitply it with 2**256-189 which is an arbitrary choosen 256
    ;; bits prime number.
    (define prga
      (let ((i 0)
            (j 0)
            (S (KSA (bytevector-copy +S+)
                    ;; A 256 bytes key
                    (apply bytevector
                           (collect from 0 to 10
                                    all (%lcg-integer 256))))))
        (lambda x
          (when (pair? x)
            (set! S (KSA (bytevector-copy +S+) (key->bytevector
                                                (number->string
                                                 (car x)))))
            ;; we have to reset `i' and `j' as well to ensure *this* new internal state will give the
            ;; same sequences given the same seed.
            (set! i 0)
            (set! j 0))
          (let* ((new-i (+mod256 i 1))
                 (new-j (+mod256 j (bytevector-u8-ref S new-i))))
            (bytevector-swap! S new-i new-j)
            (set! i new-i)
            (set! j new-j)
            (bytevector-u8-ref S (+mod256 (bytevector-u8-ref S i)
                                          (bytevector-u8-ref S j)))))))

    ;; Discard the first few bytes since the first few bytes of arc4random are low-quality.
    (repeat 10000
      (prga))

    (define set-random-seed! prga)

    ;; Generate an 64 bits integer.
    ;; If `n' is given, it will return an integer between 0 and n-1.
    (define (random-integer . n)
      (cond
       ((null? n)
        (+ (* 256 (+ (* 256 (+ (* 256 (+ (* 256 (+ (* 256 (+ (* 256 (+ (* 256 (prga))
                                                                       (prga)))
                                                             (prga)))
                                                   (prga)))
                                         (prga)))
                               (prga)))
                     (prga)))
           (prga)))
       ((< (car n) 0)
        (error "Argument must be a positive number" random-integer))
       (else
        (let ((mod-num (modulo +interger-size+ (car n))))
          (call/cc
           (lambda (k)
             (do ((i 0 0))
                 (#false)
               (let ((num-rand (random-integer)))
                 (when (<= num-rand (- +interger-size+ mod-num))
                   (k (modulo num-rand (car n))))))))))))

    ;; A random number between 0 (inclusive) and 1 (exclusive)
    (define (random)
      (/ (random-integer) +interger-size+))

    (define random-real random)

    (define (box-muller)
      (let* ((U1 (random))
             (U2 (random))
             (log-term (sqrt (* -2 (log U1))))
             (angle-term (* 2 +PI+ U2)))
        (cons (* log-term
                 (cos angle-term))
              (* log-term
                 (sin angle-term)))))

    ;; Returns a random number in the normal distribution.
    (define (random-gaussian)
      (car (box-muller)))

    (define (random-pick lst)
      (list-ref lst (random-integer (length lst))))

    ;; Fisher and Yates' modern method
    ;; https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle#Modern_method
    (define (random-shuffle lst)
      (let ((list-swap!
             ;; Swap the element of I-INDEX and J-INDEX in the list LST.
             ;; This procedure does NOT return anything, intsead, it modifies the list LST.
             (lambda (lst i-index j-index)
               (let ((tmp (list-ref lst j-index)))
                 (list-set! lst j-index (list-ref lst i-index))
                 (list-set! lst i-index tmp))))
            (len (length lst))
            (copied-list (list-copy lst)))
        (do ((i (- len 1) (- i 1)))
            ((= i 0)
             copied-list)
          (list-swap! copied-list i (random-integer (+ i 1))))))

    (define (random-true-false)
      (> (random-real) 0.5))

    ;; See `random.scm' for documentation of this procedure.
    (define (random-n-sphere dimension)
      (let* ((samples (collect from 0 to dimension all (random-gaussian)))
             (radius (apply + (map square samples))))
        (map (lambda (x) (/ x radius)) samples)))

    ))

;;;; arc4random.scm ends here.
