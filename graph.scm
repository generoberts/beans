;;;; graph.scm --- Graph algorithms -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 Gernerobert
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Pure Scheme graph algorithms.
;; Graph as a list of list of symbols. For example, ((a b c) (b c) (c a)) defined a graph whose
;; edge `a' connects to edges `b' and `c', edge `b' connects to edge `c', and edge `c' connects to
;; edge `a'.
;; TODO one-direction graph
;; TODO weighted graph

;;; Code:

(define-library (beans graph)
  (import (scheme base)
          (beans list-utils))
  (export bfs graph?)
  (begin

    ;; TODO detecting one-direction graph and weighted graph
    (define (graph? g)
      (or (and (= 1 (length g)) ; empty graph
               (null? (car g)))
          (and (list? g) ; non-empty graph
               (every list? g))))

    ;; it's the name as in the book ANSI Common Lisp, isn't it?
    ;; TODO is there a license problem?
    ;; usage: (bfs '((a b c) (b a d) (c b) (d c)) '((a)) 'd)
    (define (bfs graph queque target)
      (let* ((path (car queque))
             (node (car path)))
        (if (equal? node target)
            path
            (bfs graph
                 (append (cdr queque)
                         (map (lambda (x) (cons x path))
                              (cdr (assoc node graph))))
                 target))))))


;;;; graph.scm ends here.
