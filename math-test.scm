(test-begin "math")

(import (beans math))

(test-error (prime? 'a))
(test-error (prime? -1))
(test-error (prime? 0.5))
(test-equal #true
            (prime? 569))
(test-equal #false
            (prime? 1011))
(test-equal #true
            (prime? 1013))
(test-equal #true
            (prime? 1877))

(test-end "math")
