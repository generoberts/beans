;;;; matrix-test.scm --- Unittest for matrix.scm -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:


;;; Code:

(import (beans math matrix))

(test-begin "matrix-test")

(define row-matrix (make-matrix '((1 2 3))))
(define col-matrix (make-matrix '((1) (2) (3))))
(define full-matrix (make-matrix '((1 2 3) (4 5 6) (7 8 9))))

(test-equal '(1 . 3)
            (size row-matrix))
(test-equal '(3 . 1)
            (size col-matrix))
(test-equal '(3 . 3)
            (size full-matrix))

(test-equal 1
            (row-num row-matrix))
(test-equal 3
            (row-num col-matrix))
(test-equal 3
            (row-num full-matrix))

(test-equal 3
            (col-num row-matrix))
(test-equal 1
            (col-num col-matrix))
(test-equal 3
            (col-num full-matrix))

(test-equal '(1 2 3)
            (row-ref row-matrix 0))
(test-equal '(2)
            (row-ref col-matrix 1))
(test-equal '(7 8 9)
            (row-ref full-matrix 2))

(test-equal '(1)
            (col-ref row-matrix 0))
(test-equal '(1 2 3)
            (col-ref col-matrix 0))
(test-equal '(2 5 8)
            (col-ref full-matrix 1))

(test-equal 2
            (matrix-ref row-matrix 0 1))
(test-equal 1
            (matrix-ref col-matrix 0 0))
(test-equal 5
            (matrix-ref full-matrix 1 1))

(test-end "matrix-test")


;;;; matrix-test.scm ends here.
