;;;; random.scm --- Everything random number-related -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Here we define utility functions related to random number and stuff.
;; Since `java.util.Random' is of low quality. We use Java 17's new RNG instead.

;;; Code:

(define-library (beans random)
  (import (scheme base)
          (scheme inexact)
          (scheme case-lambda)
          (class java.util.random RandomGenerator)
          (class java.util.random RandomGeneratorFactory)
          (beans list-utils)
          (beans macro))
  (export split-random set-random-seed! random random-integer random-gaussian
          random-pick random-shuffle random-true-false random-n-sphere
          default-random-source random-real)
  (begin

    ;; an instant of random
    (define default-random-source ::RandomGenerator:SplittableGenerator (RandomGenerator:SplittableGenerator:of "L64X256MixRandom"))

    (define (split-random)
      (default-random-source:split))

    ;; setting seed is useful when we want to test stuff.
    ;; basically we recreate `default-random-source'.
    (define (set-random-seed! seed)
      (set! default-random-source
            ((RandomGeneratorFactory:of "L64X256MixRandom"):create (->long seed))))

    ;; This procedure randomly picks any number between 0 (include) to `n' (exclude), if a positive
    ;; number `n' is given. If `n' isn't given, it picks any number between 0 (include) to 1 (exclude).
    (define random
      (case-lambda
       ((x::RandomGenerator:SplittableGenerator n)
        (x:nextDouble n))
       ((x::RandomGenerator:SplittableGenerator)
        (x:nextDouble))
       ((n)
        (random default-random-source n))
       (()
        (random default-random-source))))

    (define random-real random)

    ;; Even though the random number generator here isn't LCG type, it's better, at least from our
    ;; perspective, that we should "exercise" it a bit before we use it.
    ;; The number 10000 here is arbitrary.
    (repeat 10000
      (random-real))

    (define random-gaussian
      (case-lambda
       ((x::RandomGenerator:SplittableGenerator)
        (x:nextGaussian))
       (()
        (random-gaussian default-random-source))))

    ;; randomly pick an integer between 0 to n-1 (if given).
    ;; if n isn't given, it will pick any integer between -2^63 to 2^63 (not minus 1 from both?)
    ;; the maximum value of n is 2^63 - 1 = 9,223,372,036,854,775,807
    (define random-integer
      (case-lambda
       ((x::RandomGenerator:SplittableGenerator . n)
        (cond
         ((null? n)
          (x:nextLong))
         ((< (car n) 0)
          (error "argument is not a positive number" random-integer))
         (else
          (x:nextLong (car n)))))
      (n
       (cond
        ((null? n)
         (->number (default-random-source:nextLong)))
        ((< (car n) 0)
         (error "argument is not a positive number" random-integer))
        (else
         (->number (default-random-source:nextLong (car n))))))))

    ;; picking an element from a list at random
    (define random-pick
      (case-lambda
       ((x::RandomGenerator:SplittableGenerator lst)
        (list-ref lst (random-integer x (length lst))))
       ((lst)
        (random-pick default-random-source lst))))

    ;; Fisher and Yates' modern method
    ;; https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle#Modern_method
    (define random-shuffle
      (case-lambda
       ((x::RandomGenerator:SplittableGenerator lst)
        (let ((list-swap!
               ;; Swap the element of I-INDEX and J-INDEX in the list LST.
               ;; This procedure does NOT return anything, intsead, it modifies the list LST.
               (lambda (lst i-index j-index)
                 (let ((tmp (list-ref lst j-index)))
                   (list-set! lst j-index (list-ref lst i-index))
                   (list-set! lst i-index tmp))))

              (len (length lst))
              (copied-list (list-copy lst)))
          (do ((i (- len 1) (- i 1)))
              ((= i 0)
               copied-list)
            (list-swap! copied-list i (random-integer x (+ i 1))))))
        ((lst)
         (random-shuffle default-random-source lst))))

    ;; Randomly returns either `#true' or `#false'.
    (define random-true-false
      (case-lambda
       ((x::RandomGenerator:SplittableGenerator)
        (x:nextBoolean))
       (()
        (random-true-false default-random-source))))

    ;; Generate a random point on the surface of DIMENSION dimension sphere.
    ;; Unlike in mathematics, the DIMENSION here means the number of elements in the vector represents
    ;; the point.
    ;; Note that, this procedure generates a point *on* the sphere, not in the sphere itself.
    ;; This isn't a problem in practice since in higher dimension, the volume of the sphere is close
    ;; to the surface.
    ;; Ref: https://en.wikipedia.org/wiki/N-sphere#Uniformly_at_random_on_the_(n_%E2%88%92_1)-sphere
    (define (random-n-sphere dimension)
      (let* ((samples (collect from 0 to dimension all (random-gaussian)))
             (radius (apply + (map square samples))))
        (map (lambda (x) (/ x radius)) samples)))

    ))


;;;; random.scm ends here.
