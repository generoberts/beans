;;;; nums-test.scm --- test suits for nums.scm -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:


;;; Code:

(import (beans nums))

(test-begin "nums-test")

(test-equal #true
            (ndarray? '((1 2 3))))
(test-equal #false
            (ndarray? '(1 2 3)))
(test-equal #true
            (ndarray? '((1 2) (3 4))))
(test-equal #true
            (ndarray? '((1 2) (3) (4 5 6))))

(test-equal (arange 5)
            (arange '(0 5 1)))
(test-equal '(0 0.5 1.5 2.0)
            (arange '(0 2 0.5)))

(test-equal '(0 1 2 3)
            (linspace 0 3 4))
(test-equal (arange '(0 2 0.5))
            (linspace 0 2 4))

(test-end "nums-test")

(test-begin "numpy-like-test")
;; from https://numpy.org/doc/stable/user/quickstart.html

(define a (reshap (arange 15) '(3 5)))

(test-equal '((0 1 2 3 4)
              (5 6 7 8 9)
              (10 11 12 13 14))
            a)

(test-equal '(3 5)
            (shape a))

(test-equal 2
            (ndim a))

(test-equal 15
            (size a))

(test-equal '((0 0 0 0)
              (0 0 0 0)
              (0 0 0 0))
            (zeros '(3 4)))

(test-error #true
            (ones '(2 3 4)))

(test-equal '((10 15 20 25))
            (arange '(10 30 5)))

(test-equal '((0 0.3 0.6 0.9 1.2 1.5 1.8))
            (arange '(0 2 0.3)))

(test-equal '((0 0.25 0.5 0.75 1.0 1.25 1.5 1.75 2.0))
            (linspace 0 2 9))

(test-equal '((0 1 2 3 4 5))
            (arange 6))

(test-equal '((0 1 2)
              (3 4 5)
              (6 7 8)
              (9 10 11))
            (reshap (arange 12) '(4 3)))

(test-error #true
            (reshap (arange 24) '(2 3 4)))

(test-end "numpy-like-test")

;;;; nums-test.scm ends here.
