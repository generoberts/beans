;;;; debug.scm --- Easier debugging -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Debugging by printing messages is still the most convenient and fast way to test your program,
;; but that has a flaw, too, since you can't really catch the bugs which happen in passing arguments
;; to the printing process. This library intends to catch to error and reports that the error occurs
;; in the debugging process. Also, it gives new names to printing procedures to make it easier to
;; tell that which printing procedures are for debugging and ones are not.

;;; Code:

(define-library (beans debug)
  (import (scheme base))
  (export debug-display debug-newline debug-println debug-format
          error-if-not)
  (begin

    ;; same reason as `debug-println'
    (define-syntax debug-display
      (syntax-rules ()
        ((debug-display input)
         (guard (condition
                 (else
                  (format #t "Error on calling ~a with ~a~%" debug-println (quote input))))
           (display input)))))

    ;; this doesn't have to be a macro since we call `newline' without argument, though sometime we
    ;; call it with "port" argument but that'st the cause of error if it happens.
    (define debug-newline newline)

    ;; it has to be macro since we don't the `input' be evaluated and raises an error _before_ we
    ;; catch the error with `guard'.
    (define-syntax debug-println
      (syntax-rules ()
        ((debug-println input)
         (guard (condition
                 (else
                  (format #t "Error on calling ~a with ~a~%" debug-println (quote input))))
           (format #t "~a~%" input)))))

    ;; the reason is the same as `debug-println'.
    ;; `where' is the where will we print the message to, it's the same argument as `format'.
    (define-syntax debug-format
      (syntax-rules ()
        ((debug-format where format-string arg . args)
         (guard (condition
                 (else
                  (format #t "Error on calling ~a with ~a~%" debug-format (append (list (quote arg)) (quote args)))))
           (apply format `(,where ,format-string ,@(list arg . args)))))))

    ;; A simple error checking. It's similar to `assert' in other languages.
    (define (error-if-not test text)
      (unless test
        (error text)))))


;;;; debug.scm ends here.
