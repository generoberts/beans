;;;; Version v0.8
;;;; nolib.scm --- top level code that don't belong to any library -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This file contains (probably small) top level, quality of life improvement code. Since we may want
;; to use them often, it should not belong to any library. They are "better default" functionalities.
;; of Scheme.

;;; Code:

(define (1+ . nums)
      (+ 1 (apply + nums)))

(define (1- . nums)
  (- (if (= 1 (length nums))
         ;; since (- x) is just -x. If we don't catch this case, we will have (- -x 1) which is
         ;; completely wrong.
         (car nums)
         (apply - nums))
     1))

(define (ignore . data)
  #f)

(define (always . data)
  #t)

(define (print data)
  (display data)
  (newline))

(define (printf format-string . args)
  (apply format #true format-string args))


;;;; nolib.scm ends here.
;;;; math.scm --- mathmatical utilities -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Commonly used mathematical and statistical functinos.

;;; Code:

(define-library (beans math)
  (export +PI+ +E+
          prime? *iwi-step-size*
          derivative integral integral-wide-interval root-finding diff-eq-solver partial-derivative)
  (import (scheme base)
          (scheme inexact))
  (begin

    (define +PI+ 3.141592653589793)

    (define +E+ 2.7182818284590452)


    (define +small-primes+
      (list 2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89
            97 101 103 107 109 113 127 131 137 139 149 151 157 163 167 173 179
            181 191 193 197 199 211 223 227 229 233 239 241 251 257 263 269 271
            277 281 283 293 307 311 313 317 331 337 347 349 353 359 367 373 379
            383 389 397 401 409 419 421 431 433 439 443 449 457 461 463 467 479
            487 491 499 503 509 521 523 541 547 557 563 569 571 577 587 593 599
            601 607 613 617 619 631 641 643 647 653 659 661 673 677 683 691 701
            709 719 727 733 739 743 751 757 761 769 773 787 797 809 811 821 823
            827 829 839 853 857 859 863 877 881 883 887 907 911 919 929 937 941
            947 953 967 971 977 983 991 997 1009))

    (define +last-small-primes+ (car (reverse +small-primes+)))

    ;; Try to divide `num' with small primes to speed up the process.
    ;; Returns `#true' if some small prime can divide `num'. Returns `#false' otherwise.
    (define (%try-with-small-primes num)
      (let loop ((num num)
                 (primes (cdr +small-primes+)) ; skip 2
                 (found? #false))
        (cond
         ((null? primes)
          #false)
         (found?
          #true)
         (else
          (let ((divided (/ num (car primes))))
            (if (integer? divided)
                (loop num primes #true)
                (loop num (cdr primes) #false)))))))

    ;; using the good old method to find prime
    (define (%prime? start num)
      (if (even? num)
          #false
          (let ((divided (/ num start)))
            (cond
             ((and (integer? divided)
                   (not (= start num)))
              #false)
             ;; since it's a helper function for `prime?' we don't start from 1. Thus when START and
             ;; NUM are the same, we know that only 1 and START can divide NUM so it's prime.
             ((= start num)
              #true)
             (else
              (%prime? (+ start 1) num))))))

    ;; checking prime numbers
    (define (prime? num)
      (cond
       ((not (number? num))
        (error "Not a number." prime?))
       ((or (negative? num)
            (not (integer? num)))
        (error "Prime numbers must be positive integer" prime?))
       ((<= num +last-small-primes+)
        (if (member num +small-primes+)
            #true
            #false))
       ((%try-with-small-primes num)
        #false)
       (else
        (%prime? +last-small-primes+ num))))

    ;; To calculate differential numerically, we use this constant as a limit.
    (define *dx* 0.00001)

    ;; directly from a definition of derivative
    (define (derivative f)
      (lambda (x) (/ (- (f (+ x *dx*)) (f x))
                *dx*)))

    ;; partial derivative with respect to the ith argument
    ;; f is a function that take a fix number of arguments, that is it's of the form
    ;; (lambda (x y) ...) and not (lambda z ...)
    ;; This procedure returns a procedure that accept the same number of arguments as f.
    ;; Usage example: ((partial-derivative (lambda (x y) (+ x y)) 0) 1 1)
    (define (partial-derivative f i)
      (lambda args
        (let* ((args-copy (list-copy args))
               (arg (list-ref args-copy i)))
          (set! (list-ref args-copy i) (+ arg *dx*))
          (/ (- (apply f args-copy) (apply f args))
             *dx*))))

    ;; use Simpson's 3/8 rule to evaluate integration between point a and point b.
    (define (integral f a b)
      (* (/ (- b a) 8)
         (+ (f a)
            (* 3 (f (/ (+ (* 2 a) b)
                       3)))
            (* 3 (f (/ (+ a (* 2 b))
                       3)))
            (f b))))

    (define *iwi-step-size* 0.001)
    (define (integral-wide-interval f a b)
      (unless (<= a b)
        (error "invalide range" integral-wide-interval))
      (let loop ((result 0)
                 (start a)
                 (end b))
        (if (<= end start)
            result
            (loop (+ (integral f start (+ start *iwi-step-size*)) result)
                  (+ start *iwi-step-size*)
                  end))))

    ;; find a root, a point that make the given function `f' return 0, by
    ;; Newton's method.
    (define (root-finding f)

      (define (good-enough? target guess)
        (< (abs (- target guess))
           *dx*))

      (define (next f x)
        (- x (/ (f x) ((derivative f) x))))

      (define (newton-method f guess)
        (let ((next-value (next f guess)))
          (if (good-enough? guess next-value)
              next-value
              (newton-method f next-value))))

      (newton-method f 1))

    ;;; Runge-Kutta method.
    ;; Given a function `func', an initial values `state', and a `step-size',
    ;; and returns the next state as a list of next-t and next-y.
    ;; In this implementation, `func' is a function of two variables and `state' is a list of `t' and `y'.
    (define (rk4 func state step-size)
      (let* ((t (car state))
             (y (cadr state))
             (k1 (func t y))
             (k2 (func (+ t (/ step-size 2))
                       (+ y (* step-size (/ k1 2)))))
             (k3 (func (+ t (/ step-size 2))
                       (+ y (* step-size (/ k2 2)))))
             (k4 (func (+ t step-size)
                       (+ y (* step-size k3)))))
        (list (+ t step-size)
              (+ y (/ (* step-size (+ k1
                                      (* 2 k2)
                                      (* 2 k3)
                                      k4))
                      6)))))

    ;; Solving a differential equation using Runge-Kutta method.
    ;; Take argmuntes just like `rk4', but it takes a number of `times' it should product the result.
    (define (diff-eq-solver func init-state step-size times)

      (define (%diff-eq-solver func init-state step-size times result)
        (cond
         ((zero? times)
          (reverse result))
         ((null? result)
          (let ((first-result (rk4 func init-state step-size)))
            (%diff-eq-solver func
                             first-result
                             step-size
                             (- times 1)
                             (cons first-result (cons init-state result)))))
         (else
          (let* ((previous-result (car result))
                 (this-result (rk4 func previous-result step-size)))
            (%diff-eq-solver func
                             this-result
                             step-size
                             (- times 1)
                             (cons this-result result))))))

      (%diff-eq-solver func init-state step-size times '()))))

;;;; math.scm ends here.
;;;; list-util.scm --- mini SRFI-1 (kind of) -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; We implement some functionarities from SRFI-1 and beyond that.

;;; Code:

(define-library (beans list-utils)
  (import (scheme base)
          (scheme cxr)
          (only (kawa base) future))
  (export every some any filter remove remove-if-not sublist enumerate partition combination
          fold fold-left fold-right reduce reduce-left reduce-right unfold unfold-right
          list-comparer list-equal? list-eq? list-eqv? list=
          car+cdr take drop take-right drop-right split-at last drop-last butlast
          zip unzip1 unzip2 unzip3 unzip4 unzip5
          count find find-tail
          take-while drop-while span break list-index
          delete delete-duplicates
          first second third fourth fifth sixth seventh eighth ninth tenth
          list-select composite)
  (begin

    (define (every pred lst)
      (cond
       ((null? lst)
        #true)
       ((pred (car lst))
        (every pred (cdr lst)))
       (else
        #false)))

    (define (some pred lst)
      (cond
       ((null? lst)
        #false)
       ((pred (car lst))
        #true)
       (else
        (some pred (cdr lst)))))

    (define any some)

    ;; take only what's satisfies the `pred'
    (define (filter pred lst)
      (let loop ((lst lst)
                 (result '()))
        (cond
         ((null? lst)
          (reverse result))
         ((pred (car lst))
          (loop (cdr lst)
                (cons (car lst) result)))
         (else
          (loop (cdr lst)
                result)))))

    (define remove-if-not filter)

    (define (remove pred lst)
      (filter (lambda (x) (not (pred x))) lst))

    (define (sublist lst start end)
      (let loop ((result '())
                 (count (- end start)))
        (if (= 0 count)
            result
            (loop (cons (list-ref lst (+ start (- count 1))) result)
                  (- count 1)))))

    (define (%enumerate func lst index)
      (when (not (null? lst))
        (func index (car lst))
        (%enumerate func (cdr lst) (+ index 1))))

    ;; Looping through the list `lst' where supplying both the index and the corresponding element
    ;; to `func'. `func' must be a function that take 2 arguments or more.
    (define (enumerate func lst)
      (%enumerate func lst 0))

    ;; group the list `lst' input a lits of sublists, each with `num' number of elements (except the last one).
    (define (partition num lst)
      (letrec ((%partition
                (lambda (n l r sr) ; num lst result sub-result
                  (cond
                   ((null? l)
                    (reverse (cons (reverse sr) r)))
                   ((= (length sr) n)
                    (%partition n l (cons (reverse sr) r) '()))
                   (else
                    (%partition n (cdr l) r (cons (car l) sr)))))))
        (%partition num lst '() '())))

    ;; https://cookbook.scheme.org/create-k-combinations-from-list/
    ;; Given a number `m' and a list `lst',
    ;; returs a list of all combination of `m' elements from a list `lst'.
    ;; Ex. (combination 2 '(1 2 3)) => ((1 2) (1 3) (2 3))
    ;; Note that this procedure doesn't care about the positon, that is, (1 2) is the same as (2 1)
    (define (%combine n set rest)
      (letrec ((tails-of (lambda (set)
                           (cond
                            ((null? set)
                             '())
                            (else
                             (cons set (tails-of (cdr set)))))))
               (combinations (lambda (n set)
                               (cond
                                ((zero? n)
                                 '())
                                ((= 1 n)
                                 (map list set))
                                (else
                                 (apply append
                                        (map (lambda (tail)
                                               (map (lambda (sub)
                                                      (cons (car tail) sub))
                                                    (combinations (- n 1) (rest tail))))
                                             (tails-of set))))))))
        (combinations n set)))

    (define (combination n set)
      (%combine n set cdr))

    (define (fold f init lst)
      (if (null? lst)
          init
          (fold f (f (car lst) init) (cdr lst))))

    (define fold-left fold)

    (define (fold-right f init lst)
      (if (null? lst)
          init
          (f (car lst) (fold-right f init (cdr lst)))))

    ;; According to SRFI-1
    (define (reduce f init lst)
      (if (null? lst)
          init
          (fold f (car lst) (cdr lst))))

    (define reduce-left reduce)

    (define (reduce-right f init lst)
      (if (null? lst)
          init
          (fold-right f init lst)))

    (define (unfold p f g seed . tail-gen)
      (if (null? tail-gen)
          (unfold p f g seed (lambda (x) '()))
          (if (p seed)
              ((car tail-gen) seed)
              (cons (f seed)
                    (unfold p f g (g seed) (car tail-gen))))))

    (define (unfold-right p f g seed . tail)
      (if (null? tail)
          (unfold-right p f g seed '())
          (let lp ((seed seed)
                   (lis (car tail)))
            (if (p seed)
                lis
                (lp (g seed)
                    (cons (f seed) lis))))))



    ;; compare 2 lists
    ;; This procedure compares lists and
    (define (list-comparer predicate lst1 lst2)
      (cond
       ;; the same length
       ((and (null? lst1)
             (null? lst2)))

       ;; lists of unequal length
       ((or (null? lst1)
            (null? lst2))
        #false)
       (else
        (and (predicate (car lst1) (car lst2))
             (list-comparer predicate (cdr lst1) (cdr lst2))))))

    ;; TODO FIXME
    ;; make themm accepts any number of lists to compare to.
    (define (list-equal? lst1 lst2)
      (list-comparer equal? lst1 lst2))
    (define (list-eq? lst1 lst2)
      (list-comparer eq? lst1 lst2))
    (define (list-eqv? lst1 lst2)
      (list-comparer eqv? lst1 lst2))
    (define (list= lst1 lst2)
      (list-comparer = lst1 lst2))

    (define (car+cdr p)
      (values (car p) (cdr p)))

    ;; return the first `i' elemest of the list `lst'
    (define (take lst i)
      (let loop ((result '())
                 (lst lst)
                 (index i))
        (if (zero? index)
            (reverse result)
            (loop (cons (car lst) result)
                  (cdr lst)
                  (- index 1)))))

    ;; return everything but the first `i' elemest of the list `lst'
    (define (drop lst i)
      (if (zero? i)
          lst
          (drop (cdr lst) (- i 1))))

    (define (take-right lst i)
      (drop lst (- (length lst) i)))

    (define (drop-right lst i)
      (take lst (- (length lst) i)))

    (define (split-at lst i)
      (values (take lst i)
              (drop lst i)))

    (define (last lst)
      (car (reverse lst)))

    (define (drop-last lst)
      (take lst (- (length lst) 1)))

    (define butlast drop-last)

    (define (zip lst . lsts)
      (apply map list lst lsts))

    ;; NOTE don't forget to import `cxr' library
    (define (unzip1 lst)
      (map car lst))
    (define (unzip2 lst)
      (list (unzip1 lst)
            (map cadr lst)))
    (define (unzip3 lst)
      (append (unzip2 lst)
              (map (lambda (x)
                     (car (cdr (cdr x)))) lst)))
    (define (unzip4 lst)
      (append (unzip3 lst)
              (map (lambda (x)
                     (car (cdr (cdr (cdr x))))) lst)))
    (define (unzip5 lst)
      (append (unzip4 lst)
              (map (lambda (x)
                     (car (cdr (cdr (cdr (cdr x)))))) lst)))

    ;; Using `fold' like this is better than using `reduce' as the following
    ;; (reduce + 0 (map (lambda (x) (if (pred x) 1 0))))
    ;; since we don't have to travel through the list `lst' for `map' AND `reduce'
    ;; That is, it's O(n) instead of O(n**2)
    (define (count pred lst)
      (fold (lambda (x count)
              (if (pred x)
                  (+ count 1)
                  count))
            0
            lst))

    ;; similar to `some' but instead of boolean value, it returns the value it found.
    (define (find pred lst)
      ;; code reuse from `some', only change it to make it returs `(car lst)'
      (cond
       ((null? lst)
        #false)
       ((pred (car lst))
        (car lst))
       (else
        (find pred (cdr lst)))))

    ;; similar to `find' but return the whole remaining elements of the list.
    (define (find-tail pred lst)
      ;; Again, code reuse from `some' with a tiny change.
      ;; TODO genelarize this.
      (cond
       ((null? lst)
        #false)
       ((pred (car lst))
        lst)
       (else
        (find-tail pred (cdr lst)))))

    (define (take-while pred lst)
      (if (not (pred (car lst)))
          '()
          (cons (car lst) (take-while pred (cdr lst)))))

    (define (drop-while pred lst)
      (if (pred (car lst))
          (drop-while pred (cdr lst))
          lst))

    (define (span pred lst)
      (values (take-while pred lst)
              (drop-while pred lst)))

    (define (break pred lst)
      (span (lambda (x) (not (pred x))) lst))

    (define (list-index pred . lst)
      (let loop ((index 0)
                 (all-lst lst))
        (cond
         ((some null? all-lst)
          #f)
         ((apply pred (map car all-lst))
          index)
         (else
          (loop (+ index 1)
                (map cdr all-lst))))))

    (define (delete x lst . equal)
      (if (null? equal)
          (delete x lst equal?)
          (let loop ((result '())
                     (lst lst)
                     (pred (car equal)))
            (cond
             ((null? lst)
              (reverse result))
             ((pred x (car lst))
              (loop result
                    (cdr lst)
                    pred))
             (else
              (loop (cons (car lst) result)
                    (cdr lst)
                    pred))))))

    (define (delete-duplicates lst . equal)
      (if (null? equal)
          (delete-duplicates lst equal?)
          (let loop ((acc '())
                     (lst lst)
                     (pred (car equal)))
            (cond
             ((null? lst)
              (reverse acc))
             ((member (car lst) acc pred)
              (loop acc
                    (cdr lst)
                    pred))
             (else
              (loop (cons (car lst) acc)
                    (cdr lst)
                    pred))))))

    (define first car)
    (define second cadr)
    (define third caddr)
    (define fourth cadddr)
    (define (fifth x)
      (car (cddddr x)))
    (define (sixth x)
      (cadr (cddddr x)))
    (define (seventh x)
      (caddr (cddddr x)))
    (define (eighth x)
      (cadddr (cddddr x)))
    (define (ninth x)
      (car (cddddr (cddddr x))))
    (define (tenth x)
      (cadr (cddddr (cddddr x))))

    ;; select elements of `lst' from `indexs'
    (define (list-select lst indexes)
      (if (null? indexes)
          '()
          (cons (list-ref lst (car indexes))
                (list-select lst (cdr indexes)))))

    ;; https://stackoverflow.com/questions/32782508/scheme-function-that-return-composition-of-functions
    ;; (composite f g h) => (lambda (arg) (f (g (h (args))))) and so on
    (define (composite . procs)
      (define (comp-rec arg)
        (if (null? procs)
            arg
            (let ((proc (car procs))
                  (rest (cdr procs)))
              (set! procs rest)
              (proc (comp-rec arg)))))
      comp-rec)
    ))

;;;; list-util.scm ends here.
;;;; macro.scm --- little macros -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Chicken Scheme is also has a lot of useful macro as well, we can find them at
;; http://wiki.call-cc.org/eggref/5/miscmacros
;; Even if we don't look at its code but it inspired us and it doesn't hurt to put their license in
;; `LICENSE-others' file so let's do it.

;; Chicken Scheme's Clojurian implements some interesting and useful macos from Clojure. We can find
;; its source code at https://bitbucket.org/DerGuteMoritz/clojurian/src/master/

;; Chicken Scheme's Anaphora egg implemented anaphorics macros. We take some of them which can
;; be implemented in pure R7Rs (small) Scheme. We take them from the version of "Last update: Feb 25, 2020".
;; See `LICENSE-others' file for its licenes.

;;; Code:

(define-library (beans macro)
  (import (scheme base)
          (scheme time))
  (export if-not if-let while until when-not when-let ensure
          push! pop! inc! dec!
          dotimes dolist repeat
          -> ->> if-let*
          nif nwhen ncond nwhile nand nlambda
          collect
          time)
  (begin

    (define-syntax if-not
      (syntax-rules ()
        ((if-not test then else)
         (if (not test)
             then
             else))))

    ;; evaluate `test' and bind it to `bind', if `bind' is true, then it evaluate `then', otherwise
    ;; evaluate `else'.
    (define-syntax if-let
      (syntax-rules ()
        ((if-let (bind test) then else)
         (let ((bind test))
           (if bind then else)))))

    ;; keep evaluating its body until the `test' is false.
    (define-syntax while
      (syntax-rules ()
        ((while test command ...)
         (do ((i 0 0))
             ((not test))
           (begin
             command ...)))))

    ;; like `while', but until the `test' is true.
    (define-syntax until
      (syntax-rules ()
        ((until test command ...)
         (while (not test)
                command ...))))

    (define-syntax when-not
      (syntax-rules ()
        ((when-not test command ...)
         (when (not test)
           command ...))))

    ;; evaluate `test' and bind it to `bind', if `bind' is true then it evaluates its body in the
    ;; given order.
    (define-syntax when-let
      (syntax-rules ()
        ((when-let (bind test) command ...)
         (let ((bind test))
           (when bind
             command ...)))))

    ;; evaluate `exp' and apply `predicate' to the result. If the result is true, then it returns
    ;; the result, else it signals an error.
    (define-syntax ensure
      (syntax-rules ()
        ((ensure predicate exp)
         (let ((result exp))
           (if (predicate result)
               result
               (error "Expression doesn't satisfy the predicate" ensure))))))

    ;; append `x' into a list `lst'. If `x' isn't a list, wrap it in a list and append it.
    (define-syntax push!
      (syntax-rules ()
        ((push! x lst)
         (set! lst (append lst (if (list? x) x (list x)))))))

    ;; returns the first item of the list `lst' and set the `lst' as the rest of the remaining items.
    (define-syntax pop!
      (syntax-rules ()
        ((pop! lst)
         (let ((first-element (car lst)))
           (set! lst (cdr lst))
           first-element))))

    ;; increase the given number
    (define-syntax inc!
      (syntax-rules ()
        ((inc! num)
         (set! num (+ num 1)))
        ((inc! num amount)
         (set! num (+ num amount)))))

    ;; decrease the given number
    (define-syntax dec!
      (syntax-rules ()
        ((dec! num)
         (set! num (- num 1)))
        ((dec! num amount)
         (set! num (- num amount)))))

    ;; cons-ing elements
    (define-syntax cons!
      (syntax-rules ()
        ((cons! val1 val2)
         (set! val1 (cons val1 val2)))))

    ;; append-ing `val' into `lst'
    (define-syntax append!
      (syntax-rules ()
        ((append! lst val)
         (set! lst (append lst val)))))

    ;; looping from 0 to times-1
    (define-syntax dotimes
      (syntax-rules ()
        ((dotimes (i times) body ...)
         (do ((i 0 (+ i 1)))
             ((= i times))
           body
           ...))))

    ;; not quit the same as Common Lisp one, isn't it?
    ;; looping over a list.
    ;; TODO should we support mulitple list?
    (define-syntax dolist
      (syntax-rules ()
        ((dolist (item lst) body ...)
         (for-each (lambda (item)
                     body
                     ...)
                   lst))))

    ;; keep evaluating its body for `times' times.
    (define-syntax repeat
      (syntax-rules ()
        ((repeat times command ...)
         (dotimes (i times)
                  command ...))))

    ;;; These are from Chicken Scheme's Clojurian egg (https://bitbucket.org/DerGuteMoritz/clojurian/src/master/)
    ;; originally contributed by Martin DeMello
    ;; rewritten in terms of syntax-rules by Moritz Heidkamp
    (define-syntax ->
      (syntax-rules ()
        ((-> x) x)
        ((-> x (y z ...) rest ...)
         (-> (y x z ...) rest ...))
        ((-> x y rest ...)
         (-> x (y) rest ...))))

    (define-syntax ->>
      (syntax-rules ()
        ((->> x) x)
        ((->> x (y ...) rest ...)
         (->> (y ... x) rest ...))
        ((->> x y rest ...)
         (->> x (y) rest ...))))

    (define-syntax if-let*
      (syntax-rules ()
        ((if-let* ((x y) more ...) then else)
         (car (or (and-let* ((x y) more ...)
                            (list then))
                  (list else))))))

    ;;; Anaphoric macros
    ;; R7Rs (small) Scheme can't implement `true' anaphoric macro, can it? That requires `syntax-case' which
    ;; isn't available on R7Rs (small).
    ;; Instead, we end up with `named' anaphoric macros instead.
    (define-syntax nif
      (syntax-rules ()
        ((nif name test consequent)
         (let ((name test))
           (if name consequent)))
        ((nif name test consequent alternative)
         (let ((name test))
           (if name consequent alternative)))))

    (define-syntax nwhen
      (syntax-rules ()
        ((nwhen name test xpr . xprs)
         (let ((name test))
           (if name (begin xpr . xprs))))))

    (define-syntax ncond
      (syntax-rules (else)
        ((ncond name) #f)
        ((ncond name (else xpr . xprs) . clauses)
         (let ((sym #t))
           (if sym
               (let ((name sym)) xpr . xprs)
               #f)))
        ((ncond name (test xpr . xprs) . clauses)
         (let ((sym test))
           (if sym
               (let ((name sym)) xpr . xprs)
               (ncond name . clauses))))))

    (define-syntax nwhile
      (syntax-rules ()
        ((nwhile name test xpr . xprs)
         (let loop ((name test))
           (when name
             (begin xpr . xprs)
             (loop test))))))

    (define-syntax nand
      (syntax-rules ()
        ((nand name) #t)
        ((nand name arg) arg)
        ((nand name arg0 arg1 ...)
         (let ((name arg0))
           (if name (nand name arg1 ...))))))

    (define-syntax nlambda
      (syntax-rules ()
        ((nlambda name args xpr . xprs)
         (letrec ((name (lambda args xpr . xprs)))
           name))))

    ;; Examples.
    ;; (nmap them (+ (square (car them)) (square (cadr them)) (square (caddr them))) '(1 2 3) '(2 4 6) '(3 5 7))
    ;; => (14 45 94)
    ;; (nmap them (+ (square (car them)) (square (cadr them)) (square (caddr them))) '(1) '(2 4 6) '(3 5 7))
    ;; => (14)
    ;; (nmap it (square it) '(1 2 3))
    ;; => (1 4 9)
    (define-syntax nmap
      (syntax-rules ()
        ((nmap name (head . body) lst)
         (map (lambda (name) (head . body)) lst))
        ((nmap name (head . body) lst . lists)
         (apply map (lambda name (head . body)) lst (list . lists)))))

    ;; Simple Loop-Related Macros.
    ;; You can call this macro with or without an argument for each value in this list this macro
    ;; collects. That is, you can call it either `(collect from 0 to 10)' or
    ;; `(collect from 0 to 10 each (lambda (x) (do-something-with x)))'.
    ;;
    ;; If you don't specify the argument you can pass a function that doesn't accept any argument to
    ;; the macro call, each argument in the result list will be the result of that function call.
    ;; For example, let's say a function all like (random) returns a random number, we can call this
    ;; `collect' macro with `(collect from 0 to 10 all (random))' and has a list of 10 random numbers.
    ;;
    ;; If you specify the argument to the macro, you can pass a function that accepts 1 argument to
    ;; the macro. The macro will call that function to its "indexes" when it builds the result list.
    ;; For example, `(collect from 0 to 10 each (lambda (x) (* 10 x)))' returns
    ;; (0 10 20 30 40 50 60 70 80 90) since each index, from 0 to _less than_ 10 i.e. 9, is applied to
    ;; the function `(lambda (x) (* 10 x))'.
    (define-syntax collect
      (syntax-rules (from to each)
        ((collect from this to that each func)
         (do ((i (- that 1) (- i 1))
              (result '() (cons (func i) result)))
             ((> this i)
              result)))
        ((collect from this to that all val)
         (collect from this to that each (lambda (x) val)))
        ((collect from this to that)
         (do ((i (- that 1) (- i 1))
              (result '() (cons i result)))
             ((> this i)
              result)))))

    ;; Returns a number of seconds to run the body of the macro.
    (define-syntax time
      (syntax-rules ()
        ((time head . body)
         (let ((current (current-jiffy)))
           (begin head . body)
           (inexact (/ (- (current-jiffy) current) (jiffies-per-second)))))))))

;;;; macro.scm ends here.
;;;; random.scm --- Everything random number-related -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Here we define utility functions related to random number and stuff.
;; Since `java.util.Random' is of low quality. We use Java 17's new RNG instead.

;;; Code:

(define-library (beans random)
  (import (scheme base)
          (scheme inexact)
          (scheme case-lambda)
          (class java.util.random RandomGenerator)
          (class java.util.random RandomGeneratorFactory)
          (beans list-utils)
          (beans macro))
  (export split-random set-random-seed! random random-integer random-gaussian
          random-pick random-shuffle random-true-false random-n-sphere
          default-random-source random-real)
  (begin

    ;; an instant of random
    (define default-random-source ::RandomGenerator:SplittableGenerator (RandomGenerator:SplittableGenerator:of "L64X256MixRandom"))

    (define (split-random)
      (default-random-source:split))

    ;; setting seed is useful when we want to test stuff.
    ;; basically we recreate `default-random-source'.
    (define (set-random-seed! seed)
      (set! default-random-source
            ((RandomGeneratorFactory:of "L64X256MixRandom"):create (->long seed))))

    ;; This procedure randomly picks any number between 0 (include) to `n' (exclude), if a positive
    ;; number `n' is given. If `n' isn't given, it picks any number between 0 (include) to 1 (exclude).
    (define random
      (case-lambda
       ((x::RandomGenerator:SplittableGenerator n)
        (x:nextDouble n))
       ((x::RandomGenerator:SplittableGenerator)
        (x:nextDouble))
       ((n)
        (random default-random-source n))
       (()
        (random default-random-source))))

    (define random-real random)

    ;; Even though the random number generator here isn't LCG type, it's better, at least from our
    ;; perspective, that we should "exercise" it a bit before we use it.
    ;; The number 10000 here is arbitrary.
    (repeat 10000
      (random-real))

    (define random-gaussian
      (case-lambda
       ((x::RandomGenerator:SplittableGenerator)
        (x:nextGaussian))
       (()
        (random-gaussian default-random-source))))

    ;; randomly pick an integer between 0 to n-1 (if given).
    ;; if n isn't given, it will pick any integer between -2^63 to 2^63 (not minus 1 from both?)
    ;; the maximum value of n is 2^63 - 1 = 9,223,372,036,854,775,807
    (define random-integer
      (case-lambda
       ((x::RandomGenerator:SplittableGenerator . n)
        (cond
         ((null? n)
          (x:nextLong))
         ((< (car n) 0)
          (error "argument is not a positive number" random-integer))
         (else
          (x:nextLong (car n)))))
      (n
       (cond
        ((null? n)
         (->number (default-random-source:nextLong)))
        ((< (car n) 0)
         (error "argument is not a positive number" random-integer))
        (else
         (->number (default-random-source:nextLong (car n))))))))

    ;; picking an element from a list at random
    (define random-pick
      (case-lambda
       ((x::RandomGenerator:SplittableGenerator lst)
        (list-ref lst (random-integer x (length lst))))
       ((lst)
        (random-pick default-random-source lst))))

    ;; Fisher and Yates' modern method
    ;; https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle#Modern_method
    (define random-shuffle
      (case-lambda
       ((x::RandomGenerator:SplittableGenerator lst)
        (let ((list-swap!
               ;; Swap the element of I-INDEX and J-INDEX in the list LST.
               ;; This procedure does NOT return anything, intsead, it modifies the list LST.
               (lambda (lst i-index j-index)
                 (let ((tmp (list-ref lst j-index)))
                   (list-set! lst j-index (list-ref lst i-index))
                   (list-set! lst i-index tmp))))

              (len (length lst))
              (copied-list (list-copy lst)))
          (do ((i (- len 1) (- i 1)))
              ((= i 0)
               copied-list)
            (list-swap! copied-list i (random-integer x (+ i 1))))))
        ((lst)
         (random-shuffle default-random-source lst))))

    ;; Randomly returns either `#true' or `#false'.
    (define random-true-false
      (case-lambda
       ((x::RandomGenerator:SplittableGenerator)
        (x:nextBoolean))
       (()
        (random-true-false default-random-source))))

    ;; Generate a random point on the surface of DIMENSION dimension sphere.
    ;; Unlike in mathematics, the DIMENSION here means the number of elements in the vector represents
    ;; the point.
    ;; Note that, this procedure generates a point *on* the sphere, not in the sphere itself.
    ;; This isn't a problem in practice since in higher dimension, the volume of the sphere is close
    ;; to the surface.
    ;; Ref: https://en.wikipedia.org/wiki/N-sphere#Uniformly_at_random_on_the_(n_%E2%88%92_1)-sphere
    (define (random-n-sphere dimension)
      (let* ((samples (collect from 0 to dimension all (random-gaussian)))
             (radius (apply + (map square samples))))
        (map (lambda (x) (/ x radius)) samples)))

    ))


;;;; random.scm ends here.
;;;; sort.scm --- Sorting routines -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generobert
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:


;;; Code:

(define-library (beans sort)
  (export sort quicksort quicksort* merge-sort merge-sort*)
  (import (scheme base)
          (beans list-utils))
  (begin

    ;; LST is a list of "item" to sort.
    ;; MOREN-THAN--FUNCTION is a function that accepts 2 arguments, which is an value retruned
    ;; by EXTRACT-FUNCTION applied to an "item" from the list LST and a pivot value. Such argument
    ;; is called as the 1st argument in MOREN-THAN--FUNCTION while the pivot value is called
    ;; as the 2nd argument.
    ;; EXTRACT-FUNCTION is a function to use on each "item" in the list LST.
    ;; Returns a sorted list of items
    (define (quicksort* lst more-than--function extract-function)
       ;; An empty list or a list of 1 element is already sorted
      (if (or (null? lst)
              (null? (cdr lst)))
          lst
          (let loop ((pivot (extract-function (car lst)))
                     (others (cdr lst))
                     (less-than-or-equal--list '())
                     (more-than--list '()))
            (cond
             ((null? others)
              (append (quicksort* more-than--list
                                  more-than--function
                                  extract-function)
                      (list (car lst))
                      (quicksort* less-than-or-equal--list
                                  more-than--function
                                  extract-function)))
             ((more-than--function (extract-function (car others)) pivot)
              (loop pivot
                    (cdr others)
                    less-than-or-equal--list
                    (cons (car others) more-than--list)))
             (else
              (loop pivot
                    (cdr others)
                    (cons (car others) less-than-or-equal--list)
                    more-than--list))))))

    (define (quicksort numbers)
      (quicksort* numbers > (lambda (x) x))) ; identity function

    (define (merge-sort* lst more-than--function extract-function)
      ;; it's faster to use local procedure
      (letrec ((merge
                (lambda (list1 list2 result)
                  (cond
                   ((and (null? list1)
                         (null? list2))
                    result)
                   ((null? list1)
                    (merge list1 (cdr list2) (cons (car list2) result)))
                   ((null? list2)
                    (merge (cdr list1) list2 (cons (car list1) result)))
                   (else
                    (let ((head1 (extract-function (car list1)))
                          (head2 (extract-function (car list2))))
                      (if (more-than--function head2 head1)
                          (merge list1
                                 (cdr list2)
                                 (cons (car list2) result))
                          (merge (cdr list1)
                                 list2
                                 (cons (car list1) result)))))))))
        ;; an empty list or a list with 1 element are already "sorted"
        (if (or (null? lst)
                (null? (cdr lst)))
            lst
            (let ((half (quotient (length lst) 2)))
              (reverse (merge (merge-sort* (take lst half)
                                           more-than--function
                                           extract-function)
                              (merge-sort* (drop lst half)
                                           more-than--function
                                           extract-function)
                              '()))))))

    (define (merge-sort lst)
      (merge-sort* lst > (lambda (x) x)))

    (define (sort . numbers)
      (quicksort numbers))))


;;;; sort.scm ends here.
;;;; stats.scm --- Statistical functions and methods -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:


;;; Code:

(define-library (beans math stats)
  (export z-table mean median cumsum variance standard-deviation
          correlation z-score t-score t-table
          geometric-mean root-mean-square harmonic-mean
          kurtosis skewness covariance spearman-correlation)
  (import (scheme base)
          (scheme inexact)
          (beans list-utils)
          (beans sort)
          (beans math))
  (begin

    ;;; basic formulas

    (define (mean numbers)
      (/ (apply + numbers) (length numbers)))

    (define (geometric-mean numbers)
      (let ((len (length numbers)))
        (expt (apply * numbers) (/ len))))

    (define (root-mean-square numbers)
      (let ((len (length numbers)))
        (sqrt (/ (apply + (map square numbers))
                 len))))

    (define (harmonic-mean numbers)
      (let ((len (length numbers)))
        (/ len (apply + (map (lambda (x) (/ x)) numbers)))))

    (define (median nums)
      (let ((numbers (apply sort nums)))
        (if (odd? (length numbers))
            (list-ref numbers (- (round (/ (length numbers) 2)) 1))
            (let* ((before (- (round (/ (length numbers) 2)) 1))
                   (after (+ before 1)))
              (mean (list (list-ref numbers before) (list-ref numbers after)))))))

    ;; cummulate sum of numbers
    (define (cumsum nums)
      (let loop ((result '())
                 (input nums))
        (cond
         ((null? input)
          (reverse result))
         ((null? result)
          (loop (cons (car input) result)
                (cdr input)))
         (else
          (let ((next (+ (car result)
                         (car input))))
            (loop (cons next result)
                  (cdr input)))))))

    (define (variance nums)
      (let ((mean-nums (mean nums)))
        (/ (apply + (map (lambda (x) (square (- x mean-nums))) nums))
           (- (length nums) 1))))

    (define (standard-deviation nums)
      (sqrt (variance nums)))

    (define (kurtosis nums)
      (let ((mean-nums (mean nums))
            (n (length nums)))
        (- (/ (/ (apply + (map (lambda (x) (expt (- x mean-nums) 4)) nums))
                 n)
              (square (/ (apply + (map (lambda (x) (square (- x mean-nums))) nums))
                         n)))
           3)))

    ;; this is an estimator of the population skewness but we will not use this vauel. We will use
    ;; it to calculate adjusted Fisher–Pearson standardized moment coefficient, which is a more
    ;; popular definition of sample skewness
    (define (%skewness nums)
      (let ((mean-nums (mean nums))
            (n (length nums)))
        (/ (/ (apply + (map (lambda (x) (expt (- x mean-nums) 3)) nums)) n)
           (expt (/ (apply + (map (lambda (x) (square (- x mean-nums))) nums)) (- n 1)) (/ 3 2)))))

    ;; a sample skewness.
    ;; `b1' here is the symbol [[https://en.wikipedia.org/wiki/Skewness][Wikipedia]] use so let's
    ;; use it too for consistency.
    (define (skewness nums)
      (let ((b1 (%skewness nums))
            (n (length nums)))
        (/ (* (square n) b1)
           (* (- n 1) (- n 2)))))

    ;; Finding a linear correlation between 2 sets of data.
    ;; This is Pearson correlation coefficient.
    ;; Data for testing can be found in https://en.wikipedia.org/wiki/Simple_linear_regression
    (define (correlation x-list y-list)
      (if (not (= (length x-list)
                  (length y-list)))
          (error "Two data sets don't have the same lengeth" correlation)
          (let ((x-mean (mean x-list))
                (x-sd (standard-deviation x-list))
                (y-mean (mean y-list))
                (y-sd (standard-deviation y-list)))
            (* (/ (- (length x-list) 1))
               (apply + (map (lambda (x y)
                               (* (/ (- x x-mean)
                                     x-sd)
                                  (/ (- y y-mean)
                                     y-sd)))
                             x-list
                             y-list))))))

    ;; A sample covariance.
    (define (covariance x-list y-list)
      (unless (= (length x-list)
                 (length y-list))
        (error "Two data sets don't have the same lengeth" covariance))
      (let ((mean-x (mean x-list))
            (mean-y (mean y-list)))
        (/ (apply + (map (lambda (x y)
                           (* (- x mean-x)
                              (- y mean-y)))
                         x-list
                         y-list))
           (- (length x-list) 1))))

    (define (spearman-correlation x-list y-list)
      (unless (= (length x-list)
                 (length y-list))
        (error "Two data sets don't have the same lengeth" spearman-correlation))
      (let* ((ranked-x (quicksort x-list))
             (ranked-y (quicksort y-list))
             (cor (covariance ranked-x ranked-y))
             (sd-x (standard-deviation ranked-x))
             (sd-y (standard-deviation ranked-y)))
        (/ cor (* sd-x sd-y))))

    ;;; Z statistics
    ;; We have to split the integral to smaller size to get more accurate result
    (define (%z-table z1 z2)
      (integral (lambda (x)
                  (* (/ (sqrt (* 2 +PI+)))
                     (/ (exp (/ (square x) 2)))))
                z1
                z2))

    ;; It'n one-side of the normal distribution, from 0 to z.
    ;; We recussively add z-table of smaller regions together to get more accuratet result.
    ;; The step size of 0.1 is arbitary but it gives good enought accuracy to around 6 decimal places.
    (define (z-table z)
      (if (<= z 0.1)
          (%z-table 0 z)
          (+ (%z-table (- z 0.1) z)
             (z-table (- z 0.1)))))

    (define (z-score x mean-value sd-value)
      (/ (- x mean-value)
         sd-value))

    (define (%t-statistics--gamma-term v)
      (cond
       ((and (odd? v)
             (> v 1))
        (* (/ +PI+)
           (/ (sqrt v))
           (do ((i v (- i 2))
                (result 1 (* result (/ (- i 1) (- i 2)))))
               ((= i 1) result))))
       ((and (even? v)
             (> v 1))
        (* (/ 2)
           (/ (sqrt v))
           (do ((i v (- i 2))
                (result 1 (* result (/ (- i 1) (- i 2)))))
               ((= i 2) result))))
       (else
        (error (string-append (number->string v) " is invalide degree of freedom")
               %t-statistics--gamma-term))))

    (define (%t-statistics--pdf t v)
      (* (%t-statistics--gamma-term v)
         (expt (+ 1 (/ (square t) v))
               (- (/ (+ v 1) 2)))))

    (define (%t-table t1 t2 v)
      (integral (lambda (x)
                  (%t-statistics--pdf x v))
                t1
                t2))

    (define (t-score sample-mean population-mean sample-sd sample-size)
      (/ (- sample-mean population-mean)
         (/ sample-sd
            (sqrt sample-size))))

    ;; `t' is the value of t-score we want and `v' is degree of freedom.
    ;; That is, it must be use like (t-table (t-score sample-mean population-mean sample-sd sample-size) v)
    (define (t-table t v)
      (if (<= t 0.1)
          (%t-table 0 t v)
          (+ (%t-table (- t 0.1) t v)
             (t-table (- t 0.1) v))))))


;;;; stats.scm ends here.
;;;; nums.scm --- Numpy clone -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This is a partial port of Numpy (Python library.)
;; Numpy is licensed under BSD-3-Clause.
;; Even if we said this is a `port', it's actually more like a mimic of Numpy.
;; The source of this library's behaviors is is from the Numpy's doc (https://numpy.org/doc/stable/)

;;; Code:

(define-library (beans nums)
  (import (scheme base)
          (scheme cxr)
          (beans random)
          (beans list-utils)
          (beans math stats))
  (export ndarray? arange linspace ndim shape size reshape
          zeros ones random-array ndarray-operate array-plus array-minus array-mul array-div
          ndarray-operate-unary matrix-multipliable? matrix-mul
          ndarray->list ravel transpose
          ndarray-groupping-operate array-sum array-cumsum array-min array-max
          select slice hstack vstack row-stack column-stack)
  (begin

    ;; An ndarray is a list of lists
    (define (ndarray? array)
      (and (list? array)
           (every list? array)))

    ;; if given a number, returns  a list of numbers from 0 to n - 1.
    ;; if given a list of 3 elements, the first element is starting number, the second is the ending
    ;; number, and the third is the step size.
    (define (arange n)
      (cond
       ((and (integer? n)
             (or (positive? n)
                 (zero? n)))
        (let loop ((result '())
                   (count 0))
          (if (= count n)
              (reverse result)
              (loop (cons count result)
                    (+ count 1)))))
       ((and (list? n)
             (= 3 (length n)))
        (let ((start-num (car n))
              (end-num (cadr n))
              (step (caddr n)))
          (let loop ((result '())
                     (count 0))
            (let ((result-this-iter (+ start-num (* count step))))
              (if (>= result-this-iter end-num)
                  (reverse result)
                  (loop (cons result-this-iter result)
                        (+ count 1)))))))
       (else
        (error "unknow argument" arange))))

    ;; Similar to `arange' but `arange' can't control how long the result ndarray will be.
    ;; This procedure will can be used when we want to control our result's length.
    ;; We use `(+ end step)' in the last expression since `arange' doesn't included the ending number in its result.
    ;; So we have to manually add it like that.
    (define (linspace start end len)
      (let* ((how-long (- end start))
             (step (/ how-long (- len 1))))
        (list (arange (list start (+ end step) step)))))

    (define (ndim array)
      (if (not (list? array))
          0
          (+ 1 (ndim (car array)))))

    (define (shape array)
      (cond
       ((not (list? array))
        '())
       (else
        (cons (length array) (shape (car array))))))

    (define (size array)
      (apply * (shape array)))

    ;; partion a list into a list of lists, each with `size' elements
    (define (%partition lst size)
      (let loop ((result '())
                 (current-list '())
                 (the-list lst))
        (cond
         ((and (null? the-list)
               (null? current-list))
          (reverse result))
         ((= (length current-list) size)
          (loop (cons (reverse current-list) result)
                '()
                the-list))
         (else
          (loop result
                (cons (car the-list) current-list)
                (cdr the-list))))))

    ;; from a ndarray, turns it into regular list
    (define (%flatten lst)
          (cond
           ((null? lst) '())
           ((pair? (car lst))
            (append (%flatten (car lst))
                    (%flatten (cdr lst))))
           (else
            (cons (car lst) (%flatten (cdr lst))))))

    (define (reshape array dim)
      (cond
       ;; turning any ndarray into an array of 1 dimension.
       ;; we have to wrap it in `list' since we want a ndarray as an output.
       ((and (ndarray? array)
             (= 1 (size dim)))
        (list (%flatten array)))
       ;; basically reshaping of a matrix
       ((and (ndarray? array)
             (= 2 (size dim)))
        (if (or (= -1 (cadr dim))
                (= (size array)
                   (apply * dim)))
            (let* ((flatten-array (%flatten array))
                   (result-size (/ (size array) (car dim)))
                   (result (%partition flatten-array result-size)))
              result)
            (error "Invalided size" reshape)))
      ((> (size dim) 2)
        (error "Unimplemented dimension" reshape))
       (else
        (error "Unknow error" reshape))))

    ;; like `arange' but we can specific each element by `element' which is a procedure that accepts
    ;; no arguments.
    ;; this is helpful for implementing `zeros', `ones', and list of random numbers.
    (define (%arange-specific element-gen n)
      (let loop ((result '())
                 (count 0))
        (if (= count n)
            (list (reverse result))
            (loop (cons (element-gen) result)
                  (+ count 1)))))

    (define (zeros n)
      (cond
       ((integer? n)
        (%arange-specific (lambda () 0) n))
       ((and (list? n)
             (= 2 (length n)))
        (let ((len (apply * n)))
          (reshape (%arange-specific (lambda () 0) len) n)))
       (else
        (error "invalide argument" zeros))))

    (define (ones n)
      (cond
       ((integer? n)
        (%arange-specific (lambda () 1) n))
       ((and (list? n)
             (= 2 (length n)))
        (let ((len (apply * n)))
          (reshape (%arange-specific (lambda () 1) len) n)))
       (else
        (error "invalide argument" ones))))

    (define (random-array n)
      (cond
       ((integer? n)
        (%arange-specific (lambda () (random)) n))
       ((and (list? n)
             (= 2 (length n)))
        (let ((len (apply * n)))
          (reshape (%arange-specific (lambda () (random)) len) n)))
       (else
        (error "invalide argument" random-array))))

    ;; binary operation on two arrays.
    ;; TODO this is implemented only on maximum of 2 dimesion ndarray, i.e. a matrix.
    (define (ndarray-operate pred array1 array2)
      (if (not (= (size array1)
                  (size array2)))
          (error "Can't operate on arrays of difference size" ndarray-operate)
          (map (lambda (x y)
                 (map (lambda (x-sub y-sub)
                        (pred x-sub y-sub))
                      x y))
               array1 array2)))

    ;; note that `array-mul' is element-wise multiplication.
    ;; For matrix multiplication, use `matrix-mul'.
    (define (array-plus a1 a2)
      (ndarray-operate + a1 a2))
    (define (array-minus a1 a2)
      (ndarray-operate - a1 a2))
    (define (array-mul a1 a2)
      (ndarray-operate * a1 a2))
    (define (array-div a1 a2)
      (ndarray-operate / a1 a2))

    ;; unary operation on an array.
    ;; TODO this is implemented only on maximum of 2 dimesion ndarray, i.e. a matrix.
    (define (ndarray-operate-unary pred array)
      (map (lambda (x)
             (map (lambda (x-sub)
                    (pred x-sub))
                  x))
           array))

    (define (matrix-multipliable? m1 m2)
      (= (cadr (shape m1))
         (car (shape m2))))

    (define (matrix-mul a1 a2)
      (if (not (matrix-multipliable? a1 a2))
          (error "Rank mismatch" matrix-multipliable?)
          (map
           (lambda (row)
             (apply map
                    (lambda column
                      (apply + (map * row column)))
                    a2))
           a1)))

    ;; from a array, to a plain list
    (define ndarray->list %flatten)

    ;; similar to `ndarray->list' but this returns a ndarray instead of list
    (define (ravel array)
      (list (ndarray->list array)))

    (define (transpose mtr)
      (apply map list mtr))

    ;; instead of operating on each element, we can operate on all of the element, or from each row
    ;; or from each column.
    ;; The valide values for axis are 'all, 'row, and 'column.
    (define (ndarray-groupping-operate array axis pred)
      (case axis
        ;; This is when we treat an array as a big list.
        ;; We wrap the result in `list' because we want the result to be `ndarray', not a list.
        ((all)
         (list (apply pred (ndarray->list array))))
        ((col)
         (list (map (lambda (row) (apply pred row)) array)))
        ((row)
         (list (map (lambda (col) (apply pred col)) (transpose array))))))

    (define (array-sum array axis)
      (ndarray-groupping-operate array axis +))

    (define (array-cumsum array axis)
      (ndarray-groupping-operate array axis cumsum))

    (define (array-min array axis)
      (ndarray-groupping-operate array axis min))

    (define (array-max array axis)
      (ndarray-groupping-operate array axis max))

    (define (select array row col)
      (list-ref (list-ref array row) col))

    ;; axis is 'row or 'col.
    ;; This procedure selecet only the start-th element to the (end-1)-th element of row/column axis
    ;; from the array.
    ;; we use `car' on the result because it somehow wraps the result in another list.
    ;; If it's 1darray, give `axis' an 'all.
    (define (slice array axis start end )
      (ndarray-groupping-operate array axis (lambda x (sublist x start end))))

    (define vstack append)

    (define (hstack a1 a2)
      (map (lambda (row1 row2) (append row1 row2)) a1 a2))

    ;; stack 2 1darraies into 2darray.
    ;; if a1 and a2 are already 2darray, use `hstack'.
    (define (column-stack a1 a2)
      (if (and (= 1 (car (shape a1)))
               (= 1 (car (shape a2))))
          (append a1 a2)
          (hstack a1 a2)))

    (define row-stack vstack)

    ))


;;;; nums.scm ends here.
;;;; file-utils.scm --- Common tasks that are done with files -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Here we extend the common procedures that are suitable for day-to-day tasks.

;;; Code:


(define-library (beans file-utils)
  (export readlines for-each-line write-to-file
          load-scheme-object save-scheme-object
          with-lazy-readlines)
  (import (scheme base)
          (scheme file)
          (scheme write)
          (scheme read))
  (begin

    ;; reading a file line-by-line, each line is an element in a list
    ;; By user "Will" from https://stackoverflow.com/questions/16335454/reading-from-file-using-scheme
    (define (readlines file-name)
      (call-with-input-file file-name
        (lambda (port)
          (let loop ((line (read-line port))
                     (result '()))
            (if (eof-object? line)
                (reverse result)
                (loop (read-line port) (cons line result)))))))

    ;; similar to `readlines' but in case we want to process file line-by-line without storing it in
    ;; a variable.
    ;; PROC must a procedure that accepts one argument.
    (define (for-each-line file-name proc)
      (let ((all-line (readlines file-name)))
        (for-each (lambda (line) (proc line))
                  all-line)))

    ;; writing each elment of the given list to a file
    (define (write-to-file file-name lst)
      (call-with-output-file file-name
        (lambda (port)
          (for-each (lambda (element)
                      (write element port)
                      (newline port))
                    lst))))

    ;; saving `obj' to a file to use later.
    ;; TODO already test with a list and vector, need more test. But isn't
    ;; R7Rs guarantee usage of `write' and `read' like this?
    (define (save-scheme-object file-name obj)
      (call-with-output-file file-name
        (lambda (port) (write obj port))))

    ;; usage: (define x (load-scheme-object "file-name"))
    (define (load-scheme-object file-name)
      (call-with-input-file file-name
        (lambda (port) (read port))))

    ;; reading a file `filename' line by line, each line is process using `proc' which must be a
    ;; procedure that accepts one argument.
    ;; It's good when we have to read and process each line of a large file.
    (define (with-lazy-readlines filename proc)
      (let* ((%file ::java.io.FileInputStream (java.io.FileInputStream (->java.lang.String filename)))
             (%sc ::java.util.Scanner (java.util.Scanner %file)))
        (dynamic-wind
            (lambda () #f)
            (lambda ()
              (do ((i #t)) ;; infinite loop
                  ((not (%sc:hasNextLine)))
                (proc (%sc:nextLine))))
            (lambda ()
              (%file:close)
              (%sc:close)))))))

;;;; file-utils.scm ends here.
;;;; graph.scm --- Graph algorithms -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 Gernerobert
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Pure Scheme graph algorithms.
;; Graph as a list of list of symbols. For example, ((a b c) (b c) (c a)) defined a graph whose
;; edge `a' connects to edges `b' and `c', edge `b' connects to edge `c', and edge `c' connects to
;; edge `a'.
;; TODO one-direction graph
;; TODO weighted graph

;;; Code:

(define-library (beans graph)
  (import (scheme base)
          (beans list-utils))
  (export bfs graph?)
  (begin

    ;; TODO detecting one-direction graph and weighted graph
    (define (graph? g)
      (or (and (= 1 (length g)) ; empty graph
               (null? (car g)))
          (and (list? g) ; non-empty graph
               (every list? g))))

    ;; it's the name as in the book ANSI Common Lisp, isn't it?
    ;; TODO is there a license problem?
    ;; usage: (bfs '((a b c) (b a d) (c b) (d c)) '((a)) 'd)
    (define (bfs graph queque target)
      (let* ((path (car queque))
             (node (car path)))
        (if (equal? node target)
            path
            (bfs graph
                 (append (cdr queque)
                         (map (lambda (x) (cons x path))
                              (cdr (assoc node graph))))
                 target))))))


;;;; graph.scm ends here.
;;;; matrix.scm --- Matrix math -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This is mostly a corner stone for each other math-ish libraries.
;; It use vector internally but communicates with other libraries via list.

;;; Code:

(define-library (beans math matrix)
  (import (scheme base)
          (scheme case-lambda)
          (beans macro)
          (except kawa.lib.prim_imports string) ; for `double[][]'
          (class org.ejml.simple
                 SimpleMatrix SimpleEVD SimpleSVD))
  (export make-matrix make-identity-matrix
          determinant invert psuedo-invert
          add mult hadamard-product dot kron negative matrix-apply
          row-num col-num size row-ref col-ref matrix-ref
          matrix->list solve matrix-trace transpose
          evd eigen-values eigen-vectors
          svd singular-values svd-u svd-v svd-w)
  (begin

    (define (make-matrix 2d-number-list) ::SimpleMatrix
      (let ((double-array ::double[][] (apply double[][]
                                              (map (lambda (row)
                                                     (apply double[] row))
                                                   2d-number-list))))
        (SimpleMatrix double-array)))

    (define (make-identity-matrix width) ::SimpleMatrix
      (SimpleMatrix:identity width))

    
;;; SimpleMatrix section
    (define (determinant m::SimpleMatrix)
      (m:determinant))

    (define (invert m::SimpleMatrix) ::SimpleMatrix
      (m:invert))

    ;; Moore-Penrose pseudo-inverse of the matrix.
    ;; Normally, a non-square matrix doesn't have an inverse, but it's useful to compute one when, for example,
    ;; solving an equation of polynomial regression.
    (define (psuedo-invert m::SimpleMatrix) ::SimpleMatrix
      (m:pseudoInverse))

    ;; If the 2nd argument is a matrix, add those two matrices elementwise.
    ;; If the 2nd argument is a number, add that number to each elemnt in the matrix M.
    (define add
      (case-lambda
       ((m1::SimpleMatrix m2::SimpleMatrix) ::SimpleMatrix
        (m1:plus m2))
       ((m::SimpleMatrix scalar::double) ::SimpleMatrix
        (m:plus scalar))))

    (define (mult m1::SimpleMatrix m2::SimpleMatrix) ::SimpleMatrix
      (m1:mult m2))

    (define (hadamard-product m1::SimpleMatrix m2::SimpleMatrix) ::SimpleMatrix
      (m1:elementMult m2))

    ;; Dot, a.k.a inner, product of 2 vectors.
    (define (dot v1::SimpleMatrix v2::SimpleMatrix)
      (v1:dot v2))

    ;; Kronecker product
    (define (kron m1::SimpleMatrix m2::SimpleMatrix) ::SimpleMatrix
      (m1:kron m2))

    ;; Reverse the sign of all elements in the matrix M.
    (define (negative m::SimpleMatrix) ::SimpleMatrix
      (m:negative))

    ;; Apply the procedure FUNC to all elements of matrix M and return the new matrix.
    ;; FUNC is a function that accepts 1 argument.
    ;; M is a matrix of `SimpleMatrix' type.
    (define (matrix-apply func m::SimpleMatrix) ::SimpleMatrix
      (let ((m-list (matrix->list m)))
        (make-matrix
         (map (lambda (x-row)
                (map (lambda (x)
                       (func x))
                     x-row))
              m-list))))

    ;; The number of row.
    (define (row-num m::SimpleMatrix)
      (m:numRows))

    ;; The number of column.
    (define (col-num m::SimpleMatrix)
      (m:numCols))

    ;; A pair represent a dimension of matrix
    (define (size m::SimpleMatrix)
      (cons (row-num m) (col-num m)))

    ;; The n-th row.
    (define (row-ref m::SimpleMatrix n) ::SimpleMatrix
      (m:rows n (+ n 1)))

    ;; The n-th column.
    (define (col-ref m::SimpleMatrix n) ::SimpleMatrix
      (m:cols n (+ n 1)))

    ;; The specific element in the matrix.
    (define (matrix-ref m::SimpleMatrix row-index col-index)
      (m:get row-index col-index))

    (define (matrix->list m::SimpleMatrix)
      (let* ((cols (m:numCols))
             (rows (m:numRows))
             (index-cols (collect from 0 to cols))
             (index-rows (collect from 0 to rows)))
        (map (lambda (y)
               (map (lambda (x)
                      (m:get y x))
                    index-cols))
             index-rows)))

    ;; Solve the equation aX = b.
    (define (solve a::SimpleMatrix b::SimpleMatrix) ::SimpleMatrix
      (a:solve b))

    (define (matrix-trace m::SimpleMatrix)
      (m:trace))

    (define (transpose m::SimpleMatrix) ::SimpleMatrix
      (m:transpose))

    
;;; SimpleEVD section
    (define (evd m::SimpleMatrix) ::SimpleEVD
      (m:eig))

    ;; Return a list of eigenvalues in no particular order.
    (define (eigen-values m::SimpleEVD)
      (map (lambda (x) x)
           (m:getEigenvalues)))

    ;; Return a list of SimpleMatrix of eigenvectors.
    (define (eigen-vectors m::SimpleEVD)
      (map (lambda (x)
             (m:getEigenVector x))
           (collect from 0 to (m:getNumberOfEigenvalues))))

    
;;; SimpleSVD section
    ;; SVD is of the form M = UWV^T
    (define (svd m::SimpleMatrix) ::SimpleSVD
      (m:svd))

    ;; This is the W matrix, but since it's a diagonal matrix we can use a list to store it like this.
    ;; The list is ordered from largest to smallest singular values.
    (define (singular-values m::SimpleSVD)
      (map (lambda (x) x) ; we have to map over the double[] arrray to change it to a list.
           (m:getSingularValues)))

    (define (svd-u m::SimpleSVD) ::SimpleMatrix
      (m:getU))

    (define (svd-v m::SimpleSVD) ::SimpleMatrix
      (m:getV))

    (define (svd-w m::SimpleSVD) ::SimpleMatrix
      (m:getW))
    ))


;;;; matrix.scm ends here.
;;;; string.scm --- utilities for strings -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Originally, I wanted a procedure to concate strings but consider that R7Rs (small) provides only a
;; few numbers of procedure for strings, so it's better to create my own library for these kind of tracks.


;;; Code:

(define-library (beans string)
  (export concate string-replace)
  (import (scheme base))
  (begin

    (define concate string-append)

    ;; Replacet the OLD string with the NEW string in the STR string.
    ;; Returns the new string.
    (define (string-replace str::String old new)
      (str:replaceAll old new))))


;;;; string.scm ends here.
;;;; robot.scm --- GUI automation -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This aims to be a somewhat simple GUI automation. Actually I write this library as an alternative
;; to `pyautogui' library from Python in a hope that I will use Scheme more as substition of Python
;; for everyday's tracks.
;; Most of the code is from what I alread wrote for my office work but haven't manage them into a
;; uniform and reusuable for other projects yet.

;;; Code:

(define-library (beans robot)
  (import (scheme base)
          (scheme char)
          (scheme write) ; display
          (scheme process-context) ; exit
          (only (kawa base) make sleep)
          (class java.awt Robot))
  (export mouse-right-click mouse-move mouse-location set-robot-autodelay! press-control release-control
          press-shift release-shift escape-key backspace-key enter-key countdown
          keyboard-type-character keyboard-type-string check-safe-guard
          *safe-guard-safe-location-x-axis* *safe-guard-safe-location-y-axis*)
  (begin

    (define +robot+ ::Robot (make Robot))

    ;; return the current location of the mouse in the form a pair whose `car' is x-axis location
    ;; and `cdr' is y-axis location.
    (define (mouse-location)
      (let ((x (((java.awt.MouseInfo:getPointerInfo):getLocation):getX))
            (y (((java.awt.MouseInfo:getPointerInfo):getLocation):getY)))
        (cons x y)))

    ;; interval is in millisecond
    ;; It's a delay AFTER each event
    ;; Default is 0
    (define (set-robot-autodelay! interval)
      (set! +robot+:autoDelay interval))

    ;; it's often that we want to count down as a mean for warning the user, so let's make this common thing a procedure
    (define (countdown n)
      (do ((i n (- i 1)))
          ((= i 0))
        (sleep 1)
        (display i)
        (newline)))

    ;; A safe guard is used for quick exit when something goes wrong. It does that by chceking wheter
    ;; the curretn mouse position is in the top left corner or not. If it is, then it will call `exit'
    ;; to stop the program.
    ;; By default that `top left corner' is within an area of 10x10 pixel^2 from the top left corner.
    (define *safe-guard* #true)
    (define *safe-guard-safe-location-x-axis* 10)
    (define *safe-guard-safe-location-y-axis* 10)
    (define (check-safe-guard)
      (when (and  *safe-guard*
                  (let* ((mouse-loc (mouse-location))
                         (x-loc (car mouse-loc))
                         (y-loc (cdr mouse-loc)))
                    (and (<= x-loc *safe-guard-safe-location-x-axis*)
                         (<= y-loc *safe-guard-safe-location-y-axis*))))
        (display "Safe guard is triggered. Exit.")
        (exit)))

    (define (mouse-right-click)
      (check-safe-guard)
      (+robot+:mousePress java.awt.event.MouseEvent:BUTTON1_DOWN_MASK)
      (+robot+:mouseRelease java.awt.event.MouseEvent:BUTTON1_DOWN_MASK))

    (define (mouse-move x y)
      (check-safe-guard)
      (+robot+:mouseMove x y))

    (define (press-control)
      (check-safe-guard)
      (+robot+:keyPress java.awt.event.KeyEvent:VK_CONTROL))
    (define (release-control)
      (check-safe-guard)
      (+robot+:keyRelease java.awt.event.KeyEvent:VK_CONTROL))
    (define (press-shift)
      (check-safe-guard)
      (+robot+:keyPress java.awt.event.KeyEvent:VK_SHIFT))
    (define (release-shift)
      (check-safe-guard)
      (+robot+:keyRelease java.awt.event.KeyEvent:VK_SHIFT))
    (define (escape-key)
      (check-safe-guard)
      (+robot+:keyPress java.awt.event.KeyEvent:VK_ESCAPE)
      (+robot+:keyRelease java.awt.event.KeyEvent:VK_ESCAPE))
    (define (backspace-key)
      (check-safe-guard)
      (+robot+:keyPress java.awt.event.KeyEvent:VK_BACK_SPACE)
      (+robot+:keyRelease java.awt.event.KeyEvent:VK_BACK_SPACE))
    (define (enter-key)
      (check-safe-guard)
      (+robot+:keyPress java.awt.event.KeyEvent:VK_ENTER)
      (+robot+:keyRelease java.awt.event.KeyEvent:VK_ENTER))

    ;; snippet to print out those A-Z case. Just in case we want it again.
    ;; Other keys can be done the same way.
    ;; (do ((i 0 (+ i 1)))
    ;; ((= i 26))
    ;; (format #true "((#\\~a)~%(+robot+:keyPress java.awt.event.KeyEvent:VK_~a)~%(+robot+:keyRelease java.awt.event.KeyEvent:VK_~a))~%" (string-downcase (string (integer->char (+ i 65)))) (string (integer->char (+ i 65))) (string (integer->char (+ i 65)))))
    (define (keyboard-type-character charact)
      (check-safe-guard)
      (unless (char? charact)
        (error "Need a character" keyboard-type-character))
      (case charact
        ((#\0)
		 (+robot+:keyPress java.awt.event.KeyEvent:VK_0)
		 (+robot+:keyRelease java.awt.event.KeyEvent:VK_0))
	    ((#\1)
		 (+robot+:keyPress java.awt.event.KeyEvent:VK_1)
		 (+robot+:keyRelease java.awt.event.KeyEvent:VK_1))
	    ((#\2)
		 (+robot+:keyPress java.awt.event.KeyEvent:VK_2)
		 (+robot+:keyRelease java.awt.event.KeyEvent:VK_2))
	    ((#\3)
		 (+robot+:keyPress java.awt.event.KeyEvent:VK_3)
		 (+robot+:keyRelease java.awt.event.KeyEvent:VK_3))
	    ((#\4)
		 (+robot+:keyPress java.awt.event.KeyEvent:VK_4)
		 (+robot+:keyRelease java.awt.event.KeyEvent:VK_4))
	    ((#\5)
		 (+robot+:keyPress java.awt.event.KeyEvent:VK_5)
		 (+robot+:keyRelease java.awt.event.KeyEvent:VK_5))
	    ((#\6)
		 (+robot+:keyPress java.awt.event.KeyEvent:VK_6)
		 (+robot+:keyRelease java.awt.event.KeyEvent:VK_6))
	    ((#\7)
		 (+robot+:keyPress java.awt.event.KeyEvent:VK_7)
		 (+robot+:keyRelease java.awt.event.KeyEvent:VK_7))
	    ((#\8)
		 (+robot+:keyPress java.awt.event.KeyEvent:VK_8)
		 (+robot+:keyRelease java.awt.event.KeyEvent:VK_8))
	    ((#\9)
		 (+robot+:keyPress java.awt.event.KeyEvent:VK_9)
		 (+robot+:keyRelease java.awt.event.KeyEvent:VK_9))
        ((#\a)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_A)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_A))
        ((#\b)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_B)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_B))
        ((#\c)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_C)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_C))
        ((#\d)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_D)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_D))
        ((#\e)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_E)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_E))
        ((#\f)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_F)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_F))
        ((#\g)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_G)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_G))
        ((#\h)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_H)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_H))
        ((#\i)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_I)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_I))
        ((#\j)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_J)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_J))
        ((#\k)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_K)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_K))
        ((#\l)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_L)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_L))
        ((#\m)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_M)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_M))
        ((#\n)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_N)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_N))
        ((#\o)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_O)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_O))
        ((#\p)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_P)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_P))
        ((#\q)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_Q)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_Q))
        ((#\r)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_R)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_R))
        ((#\s)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_S)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_S))
        ((#\t)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_T)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_T))
        ((#\u)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_U)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_U))
        ((#\v)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_V)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_V))
        ((#\w)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_W)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_W))
        ((#\x)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_X)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_X))
        ((#\y)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_Y)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_Y))
        ((#\z)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_Z)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_Z))
        ((#\A)
         (press-shift)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_A)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_A)
         (release-shift))
        ((#\B)
         (press-shift)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_B)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_B)
         (release-shift))
        ((#\C)
         (press-shift)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_C)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_C)
         (release-shift))
        ((#\D)
         (press-shift)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_D)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_D)
         (release-shift))
        ((#\E)
         (press-shift)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_E)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_E)
         (release-shift))
        ((#\F)
         (press-shift)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_F)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_F)
         (release-shift))
        ((#\G)
         (press-shift)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_G)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_G)
         (release-shift))
        ((#\H)
         (press-shift)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_H)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_H)
         (release-shift))
        ((#\I)
         (press-shift)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_I)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_I)
         (release-shift))
        ((#\J)
         (press-shift)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_J)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_J)
         (release-shift))
        ((#\K)
         (press-shift)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_K)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_K)
         (release-shift))
        ((#\L)
         (press-shift)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_L)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_L)
         (release-shift))
        ((#\M)
         (press-shift)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_M)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_M)
         (release-shift))
        ((#\N)
         (press-shift)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_N)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_N)
         (release-shift))
        ((#\O)
         (press-shift)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_O)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_O)
         (release-shift))
        ((#\P)
         (press-shift)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_P)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_P)
         (release-shift))
        ((#\Q)
         (press-shift)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_Q)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_Q)
         (release-shift))
        ((#\R)
         (press-shift)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_R)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_R)
         (release-shift))
        ((#\S)
         (press-shift)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_S)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_S)
         (release-shift))
        ((#\T)
         (press-shift)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_T)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_T)
         (release-shift))
        ((#\U)
         (press-shift)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_U)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_U)
         (release-shift))
        ((#\V)
         (press-shift)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_V)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_V)
         (release-shift))
        ((#\W)
         (press-shift)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_W)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_W)
         (release-shift))
        ((#\X)
         (press-shift)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_X)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_X)
         (release-shift))
        ((#\Y)
         (press-shift)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_Y)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_Y)
         (release-shift))
        ((#\Z)
         (press-shift)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_Z)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_Z)
         (release-shift))
        ((#\.)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_PERIOD)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_PERIOD))
        ((#\*)
         (press-shift)
         (keyboard-type-character #\8)
         (release-shift))
        ((#\@)
         (press-shift)
         (keyboard-type-character #\2)
         (release-shift))
        ((#\\)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_BACK_SLASH)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_BACK_SLASH))
        ((#\()
         (press-shift)
         (keyboard-type-character #\9)
         (release-shift))
        ((#\))
         (press-shift)
         (keyboard-type-character #\0)
         (release-shift))
        ((#\[)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_OPEN_BRACKET)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_OPEN_BRACKET))
        ((#\])
         (+robot+:keyPress java.awt.event.KeyEvent:VK_CLOSE_BRACKET)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_CLOSE_BRACKET))
        ((#\:)
         (press-shift)
         (keyboard-type-character #\;)
         (release-shift))
        ((#\,)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_COMMA)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_COMMA))
        ((#\$)
         (press-shift)
         (keyboard-type-character #\4)
         (release-shift))
        ((#\=)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_EQUALS)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_EQUALS))
        ((#\!)
         (press-shift)
         (keyboard-type-character #\1)
         (release-shift))
        ((#\-)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_MINUS)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_MINUS))
        ((#\#)
         (press-shift)
         (keyboard-type-character #\3)
         (release-shift))
        ((#\+)
         (press-shift)
         (keyboard-type-character #\=)
         (release-shift))
        ((#\;)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_SEMICOLON)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_SEMICOLON))
        ((#\/)
         (+robot+:keyPress java.awt.event.KeyEvent:VK_SLASH)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_SLASH))
        ((#\ )
         (+robot+:keyPress java.awt.event.KeyEvent:VK_SPACE)
         (+robot+:keyRelease java.awt.event.KeyEvent:VK_SPACE))
        ((#\_)
         (press-shift)
         (keyboard-type-character #\-)
         (release-shift))
        (else
         (error "Unimplemented character" keyboard-type-character))))

    (define (keyboard-type-string str)
      (check-safe-guard)
      (let ((chars (string->list str)))
        (for-each (lambda (c)
                    (keyboard-type-character c))
                  chars)))
    ))



;;;; robot.scm ends here.
;;;; csv.scm --- Working with csv files -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Easy to use csv files manipulator.

;;; Code:

(define-library (beans csv)
  (export read-csv read-csv* write-csv! write-csv*!)
  (import (scheme base)
          (scheme write)
          (scheme file)
          (beans file-utils)
          (beans macro)
          (kawa lib strings_ext))
  (begin

    ;; TODO use lazy reading to process each line and add them into the list which will be returned
    ;; from `read-csv'
    ;; The maximum filesize we can read at one go.
    ;; (define +filesize-limit+ )

    (define (%write-list-to-csv port lst)
      (for-each (lambda (item)
                  (write item port)
                  (display "," port))
                lst))

    ;; sometime, it's better to use `display' instead of `write'
    (define (%write-list-to-csv* port lst)
      (for-each (lambda (item)
                  (display item port)
                  (display "," port))
                lst))

    ;; write a list `lst' to `filename'. `lst' must be a list of lists.
    (define (write-csv! filename lst)
      (call-with-output-file filename
        (lambda (port)
          (for-each (lambda (row)
                      (%write-list-to-csv port row)
                      (newline port))
                    lst))))

    ;; an analog to `write-csv!' which use `display' instead of `write'
    (define (write-csv*! filename lst)
      (call-with-output-file filename
        (lambda (port)
          (for-each (lambda (row)
                      (%write-list-to-csv* port row)
                      (newline port))
                    lst))))

    ;; retrun a list of list of each row of CSV file.
    ;; it's like `read-csv' but it doesn't try to convert each element to a number
    (define (read-csv* filename)
      (->> (readlines filename)
           (map (lambda (row)
                  (string-split row ",")))))

    ;; retrun a list of list of each row of CSV file.
    ;; each row is splitted by "," and each element is then converted to number (if it can)
    (define (read-csv filename)
      (->> (read-csv* filename)
           (map (lambda (row)
                  (map (lambda (element)
                         (let ((converted (string->number (string-trim element))))
                           (if converted
                               converted
                               element)))
                       row)))))))

;;;; csv.scm ends here.
;;;; regression.scm --- Regression analysis -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:


;;; Code:

(define-library (beans regression)
  (export linear-regression linear-prediction-at-point linear-prediction-from-point)
  (import (scheme base)
          (scheme inexact))
  (begin

    ;; The linear equation is Y = a + bX + error
    ;; This procedure returns, in this order, a, b, standard error of error term, standdard error of a,
    ;; stardard error of b.
    ;; TODO need t-statistics to calculate the confident intervals.
    ;; Data for testing can be found in https://en.wikipedia.org/wiki/Simple_linear_regression
    ;; series x is 1.47 1.50 1.52 1.55 1.57 1.60 1.63 1.65 1.68 1.70 1.73 1.75 1.78 1.80 1.83
    ;; series y is 52.21 53.12 54.48 55.84 57.20 58.57 59.93 61.29 63.11 64.47 66.28 68.10 69.92 72.19 74.46
    (define (linear-regression x-series y-series)
      (let ((lenx (length x-series))
            (leny (length y-series))
            (sum-square (lambda (series) (apply + (map (lambda (x) (square x)) series))))
            (sum-multiple (lambda (x-series y-series)
                            (apply + (map (lambda (x y) (* x y))
                                          x-series
                                          y-series))))
            (sum (lambda (series) (apply + series))))
        (unless (= lenx leny)
          (error "X and Y data range mismatch" linear-regression))
        (let* ((sx (sum x-series))
               (sy (sum y-series))
               (sxx (sum-square x-series))
               (syy (sum-square y-series))
               (sxy (sum-multiple x-series y-series))
               (b (/ (- (* lenx sxy)
                        (* sx sy))
                     (- (* lenx sxx)
                        (square sx))))
               (a (- (/ sy lenx)
                     (* b (/ sx lenx))))
               (variance-error-error-term (/ (- (* lenx syy)
                                                (square sy)
                                                (* (square b)
                                                   (- (* lenx sxx)
                                                      (square sx))))
                                             (* lenx (- lenx 2))))
               (variance-error-b-term (/ (* lenx variance-error-error-term)
                                         (- (* lenx sxx)
                                            (square sx))))
               (variance-error-a-term (/ (* variance-error-b-term sxx)
                                         lenx)))
          (values a b (sqrt variance-error-error-term) (sqrt variance-error-a-term) (sqrt variance-error-b-term)))))

    (define (linear-prediction-at-point a b x)
      (+ a (* b x)))

    ;; from a single point `x', return a list of length `num' that contains values from the prediction
    ;; using `a' and `b', each of which comes from the previous predicted value by this method.
    ;; That is, we give x1, this procedure will return a list of a + bx1, a + b(a + bx1),
    ;; a + b(a + b(a + bx1)), etc.
    ;; It's useful when our model is time serise of the form Xn = a + bXn-1
    (define (linear-prediction-from-point a b x num)
      (let loop ((count 0)
                 (result '()))
        (cond
         ((= count num)
          (reverse result))
         ((null? result)
          (loop (+ count 1)
                (cons (linear-prediction-at-point a b x) result)))
         (else
          (loop (+ count 1)
                (cons (linear-prediction-at-point a b (car result)) result))))))))


;;;; regression.scm ends here.
;;;; patmat.scm --- Patter Matching -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Basic patter matching support.

;; This patter matching has 2 type of matching supported; single value matching and list matching.
;; For either of them, `otherwise' case will always get matched, and using underscore, _, as a
;; wildcard matching. We can also use custom predicate function. A custom predicate function accepts
;; 2 argument, and will use the value we want to match with as the first argument while using the
;; case's value as the second argument to the custom predicate function.

;; There are 2 type of matching; a single value matching and a list matching.
;; A single value matching does _not_ wrap its cases into lists.
;; (match x
;;       (1 'some-thing) ; match if and only if x is 1
;;       (_ 'other-thing) ; match with any value of x
;;       (otherwise 'last-thing)) ; match with any vaule of x, like using _
;; To use a custome comparator, use `using' keyword after the value you want to match.
;; (match x using my-function
;;       (1 'some-thing) ; match if and only if (my-func x 1) is true
;;       (_ 'other-thing) ; always match
;;       (otherwise 'last-thing)) ; always match as well.
;; The above examples use a number, but we can use a symbol as well. Though that will generate an
;; warning message but it'll work fine.

;; A list matching wraps each case in a list, and you can use _ to skip the element you don't care
;; about. The value you want to match with must be a list.
;; (match my-list
;;        ((_ 2 3) 'some-thing) ; `my-list' must have 2 and 3 as its 2nd and 3rd elements
;;        ((1 _ 3) 'some-other-thing) ; _ can be used in the middle
;;        ((1 2 3) 'more-thing) ; `my-list' must be '(1 2 3)
;;        ((_ _ _) 'all-thing) ; don't care about all the element
;;        (_ 'other-thing) ; we can mix sytle, too
;;        ((_) 'other-more-thing) ; or wrap it up to make it looks uniformly
;;        (otherwise 'last-thing)) ; "default" from, just for a fallback case
;; you can use `using' as well. It'll match each elment of "my-list" to, say, '(1 2 3), from left to
;; right. The element from "my-list" will be the 1st argument to the custom predicate function while
;; the element from '(1 2 3) will be the 2nd argument for the custom predicate function.
;; We can also use a variable instead of a list '(1 2 3) and the like as well. Ex.
;; (match my-list
;;       (my-other-list 'something))
;; Where `my-other-list' is a list.

;;; Notes:
;; `match' can cause side-effect.
;; `otherwise' doesn't need to be the final case, no other cases, below `otherwise' case,  will be
;; checked and run.
;; If custom predicate functionn isn't given, `match' will use `equal?'

;;; Code:

(define-library (beans patmat)
  (import (scheme base))
  (export match)

  (begin
    (define-syntax match
      (syntax-rules (otherwise using _)

        ;; cases of `otherwise'
        ((match val using comparator (otherwise body ...) another-body ...)
         (begin
           body ...))
        ((match val (otherwise body ...) another-body ...)
         (match val using equal? (otherwise body ...)))
        ((match (otherwise body ...) another-body ...)
         (begin
           body ...))


        ;; cases when there is no body
        ((match val using comparator)
         (comparator val val))
        ((match val)
         (match val using equal?))


        ;;; match a list
        ;; we recursively match until the end of the list, which means this list matches the patter.
        ((match val using comparator (() pat-body ...) body ...)
         (begin
           pat-body ...))
        ((match val using comparator ((_ pat-head-2 ...) pat-body ...) body ...)
         (match (cdr val) using comparator ((pat-head-2 ...) pat-body ...) body ...))
        ((match val using comparator ((pat-head pat-head-2 ...) pat-body ...) body ...)
         (if (comparator (car val) pat-head)
             (match (cdr val) using comparator ((pat-head-2 ...) pat-body ...) body ...)
             (match val using comparator
                    body ...)))


        ;;; match a single value
        ((match val using comparator (_ pat-body ...) body ...)
         (begin
           pat-body ...))
        ((match val using comparator (pat-head pat-body ...) body ...)
         (if (or (comparator val (quote pat-head))
                 (comparator val pat-head))
             (begin
               pat-body ...)
             (match val using comparator
                    body ...)))

        ;; case when not supply the custom comparator function
        ((match val (pat-head pat-body ...) body ...)
         (match val using equal? (pat-head pat-body ...) body ...))))))


;;;; patmat.scm ends here.
;;;; debug.scm --- Easier debugging -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Debugging by printing messages is still the most convenient and fast way to test your program,
;; but that has a flaw, too, since you can't really catch the bugs which happen in passing arguments
;; to the printing process. This library intends to catch to error and reports that the error occurs
;; in the debugging process. Also, it gives new names to printing procedures to make it easier to
;; tell that which printing procedures are for debugging and ones are not.

;;; Code:

(define-library (beans debug)
  (import (scheme base))
  (export debug-display debug-newline debug-println debug-format
          error-if-not)
  (begin

    ;; same reason as `debug-println'
    (define-syntax debug-display
      (syntax-rules ()
        ((debug-display input)
         (guard (condition
                 (else
                  (format #t "Error on calling ~a with ~a~%" debug-println (quote input))))
           (display input)))))

    ;; this doesn't have to be a macro since we call `newline' without argument, though sometime we
    ;; call it with "port" argument but that'st the cause of error if it happens.
    (define debug-newline newline)

    ;; it has to be macro since we don't the `input' be evaluated and raises an error _before_ we
    ;; catch the error with `guard'.
    (define-syntax debug-println
      (syntax-rules ()
        ((debug-println input)
         (guard (condition
                 (else
                  (format #t "Error on calling ~a with ~a~%" debug-println (quote input))))
           (format #t "~a~%" input)))))

    ;; the reason is the same as `debug-println'.
    ;; `where' is the where will we print the message to, it's the same argument as `format'.
    (define-syntax debug-format
      (syntax-rules ()
        ((debug-format where format-string arg . args)
         (guard (condition
                 (else
                  (format #t "Error on calling ~a with ~a~%" debug-format (append (list (quote arg)) (quote args)))))
           (apply format `(,where ,format-string ,@(list arg . args)))))))

    ;; A simple error checking. It's similar to `assert' in other languages.
    (define (error-if-not test text)
      (unless test
        (error text)))))


;;;; debug.scm ends here.
;;;; fft.scm --- Fast Fourier Transfrom -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This is an implementation of Cooley-Tukey's Fast Fourier Transformations algorithm.

;;; Code:

(define-library (beans math fft)
  (import (scheme base)
          (scheme inexact)
          (scheme complex)
          (beans macro)
          (beans list-utils)
          (beans math))
  (export fft fft->mag fft->magnitude fft->angle fft->degree fft->real fft->imag)
  (begin

    (define (spit-even-odd-terms x)
      (let loop ((even-terms '())
                 (odd-terms '())
                 (input x)
                 (even-turn? #true)) ; the index 0 is even index
        (cond
         ((null? input)
          (values (reverse even-terms)
                  (reverse odd-terms)))
         (even-turn?
          (loop (cons (car input) even-terms)
                odd-terms
                (cdr input)
                #false))
         (else
          (loop even-terms
                (cons (car input) odd-terms)
                (cdr input)
                #true)))))

    ;; pad the list with 0s to the length of power of 2
    (define (padding-list lst)
      (let* ((current-length (length lst))
             ;; the smallest power of two that is less than current length
             (log2-of-length (log current-length 2)))
        ;; it's already a power of 2, so just return the list `lst'
        (if (integer? log2-of-length)
            lst
            (let* ((power-of-two-before (exact (floor log2-of-length)))
                   (next-power-of-two (+ 1 power-of-two-before))
                   ;; how many more we have to pad to get to the next power of two
                   (pad-length (- (expt 2 next-power-of-two) current-length)))
              (append lst
                      (collect from 0 to pad-length all 0))))))

    ;; NOTE this procedures that the length of `x' is a power of 2.
    (define (%fft x)
      (let ((N (length x)))
        (if (= N 1)
            x
            (let-values (((even-terms-raw odd-terms-raw)
                          (spit-even-odd-terms x)))
              (let* ((even-terms (%fft even-terms-raw))
                     (odd-terms (%fft odd-terms-raw))
                     (T (collect from 0 to (/ N 2)
                                 each (lambda (k)
                                        (* (expt +E+ (/ (* -2i +PI+ k) N))
                                           (list-ref odd-terms k))))))
                (append (collect from 0 to (/ N 2)
                                 each (lambda (k)
                                        (+ (list-ref even-terms k)
                                           (list-ref T k))))
                        (collect from 0 to (/ N 2)
                                 each (lambda (k)
                                        (- (list-ref even-terms k)
                                           (list-ref T k))))))))))

    (define (fft x)
      (%fft (padding-list x)))

    (define (fft->mag x)
      (map magnitude (fft x)))

    (define fft->magnitude fft->mag)

    (define (fft->angle x)
      (map angle (fft x)))

    ;; "Normal" angle that a circle has 360 degree.
    ;; We don't map over `fft->angle' again because we don't want to travel all the way over the
    ;; result list again.
    (define (fft->degree x)
      (map (lambda (x)
             (/ (* (angle x) 360) (* 2 +PI+)))
           (fft x)))

    (define (fft->real x)
      (map real-part (fft x)))

    (define (fft->imag x)
      (map imag-part (fft x)))))


;;;; fft.scm ends here.
;;;; arc4random.scm --- RC4 random number generator -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; An implementation of RC4 cipher, for use as a random number generator.
;; RC4 consists of Key-scheduling algorithm (KSA) and Pseudo-random generation algorithm (PRGA).
;; The pseudocode for them are given below.

;; The reason for this library is that, the library `(beans random)` relies *too much* on Apacahe's
;; Common RNG, a library of JVM. Thus, it isn't portable. While we can use linear congruent generator,
;; LCG for short, to generate random numbers, its statistical properties isn't *that* good. RC4, though
;; it's a broken algorithm and has some bias, has much better statistical properties.
;; However, RC4 is slower than LCG, sometimes it's even 10 times slower. Consider these trade off when
;; you choose a random number generator for your use case.

;; NOTE: This library generates 64 bits random numbers.
;; NOTE: This library is seedable, to set a seed, call `(set-random-seed! you-seed)' where `your-seed'
;; is a number you want to use as a seed. After you set a new seed, you should also discard the first
;; few bytes by calling (prga) several times (like a thousand times or more).

;; Psudo code for RC4:
;; KSA:
;; for i from 0 to 255
;;     S[i] := i
;; endfor
;; j := 0
;; for i from 0 to 255
;;     j := (j + S[i] + key[i mod keylength]) mod 256
;;     swap values of S[i] and S[j]
;; endfor
;;
;; PRGA:
;; i := 0
;; j := 0
;; while GeneratingOutput:
;;     i := (i + 1) mod 256
;;     j := (j + S[i]) mod 256
;;     swap values of S[i] and S[j]
;;     K := S[(S[i] + S[j]) mod 256]
;;     output K
;; endwhile

;; Code:

(define-library (beans random arc4random)
  (import (scheme base)
          (scheme time)
          (scheme inexact)
          (beans macro)
          (beans list-utils)
          (only (beans math) +PI+))
  (export set-random-seed! random-integer random random-gaussian random-pick random-shuffle
          random-real random-true-false random-n-sphere)
  (begin

    ;; A simple "random" number generator to use to generate the seed of `arc4random'.
    (define word-size (- (expt 2 64) 59))

    (define %lcg
      (let ((a 18263440312458789471)
            (m word-size)
            (seed (current-jiffy)))
        (lambda new-seed
          (if (= (length new-seed) 1)
              (set! seed (car new-seed))
              (set! seed (modulo (* seed a) m)))
          (modulo seed m))))

    ;; Normally, the first few outputs of a random number generator aren't that good, so we have to
    ;; "exercise" it a bit.
    ;; We call it 10000 times, which is arbitrary, and hope that the quality will be better.
    (repeat 10000
      (%lcg))

    (define (%random-int-in-range n)
      (let ((mod (modulo word-size n)))
        (call/cc
         (lambda (k)
           (do ((i 0 0))
               (#false)
             (let ((num-rand (%lcg)))
               (when (<= num-rand (- word-size mod))
                 (k (modulo num-rand n)))))))))

    (define (%lcg-integer . n)
      (if (null? n)
          (%lcg)
          (%random-int-in-range (car n))))

    ;; We are making a 64 bits at a time using `random-integer'.
    (define +interger-size+ (expt 2 64))

    (define +S+ (apply bytevector (collect from 0 to 256)))

    ;; Swap the `i'th byte and `j'th byte in bytevector `bv'.
    (define (bytevector-swap! bv i j)
      (let ((tmp (bytevector-u8-ref bv i)))
        (bytevector-u8-set! bv i (bytevector-u8-ref bv j))
        (bytevector-u8-set! bv j tmp)))

    ;; Sum two number, `m' and `n', and take the result mod 256.
    (define (+mod256 m n)
      (modulo (+ m n) 256))

    ;; Turns a key string into a bytevector.
    (define key->bytevector string->utf8)

    (define (KSA s-array key)
      (let ((result s-array)
            (j 0)
            (key-length (bytevector-length key)))
        (do ((i 0 (+ i 1)))
            ((= i 256) result)
          (set! j (modulo (+ j (bytevector-u8-ref result i) (bytevector-u8-ref key (modulo i key-length))) 256))
          (bytevector-swap! result i j))))

    ;; `current-jiffy' is an implementation-depended number, so we can't count on that to produce a large
    ;; number of bits for the key. So we mulitply it with 2**256-189 which is an arbitrary choosen 256
    ;; bits prime number.
    (define prga
      (let ((i 0)
            (j 0)
            (S (KSA (bytevector-copy +S+)
                    ;; A 256 bytes key
                    (apply bytevector
                           (collect from 0 to 10
                                    all (%lcg-integer 256))))))
        (lambda x
          (when (pair? x)
            (set! S (KSA (bytevector-copy +S+) (key->bytevector
                                                (number->string
                                                 (car x)))))
            ;; we have to reset `i' and `j' as well to ensure *this* new internal state will give the
            ;; same sequences given the same seed.
            (set! i 0)
            (set! j 0))
          (let* ((new-i (+mod256 i 1))
                 (new-j (+mod256 j (bytevector-u8-ref S new-i))))
            (bytevector-swap! S new-i new-j)
            (set! i new-i)
            (set! j new-j)
            (bytevector-u8-ref S (+mod256 (bytevector-u8-ref S i)
                                          (bytevector-u8-ref S j)))))))

    ;; Discard the first few bytes since the first few bytes of arc4random are low-quality.
    (repeat 10000
      (prga))

    (define set-random-seed! prga)

    ;; Generate an 64 bits integer.
    ;; If `n' is given, it will return an integer between 0 and n-1.
    (define (random-integer . n)
      (cond
       ((null? n)
        (+ (* 256 (+ (* 256 (+ (* 256 (+ (* 256 (+ (* 256 (+ (* 256 (+ (* 256 (prga))
                                                                       (prga)))
                                                             (prga)))
                                                   (prga)))
                                         (prga)))
                               (prga)))
                     (prga)))
           (prga)))
       ((< (car n) 0)
        (error "Argument must be a positive number" random-integer))
       (else
        (let ((mod-num (modulo +interger-size+ (car n))))
          (call/cc
           (lambda (k)
             (do ((i 0 0))
                 (#false)
               (let ((num-rand (random-integer)))
                 (when (<= num-rand (- +interger-size+ mod-num))
                   (k (modulo num-rand (car n))))))))))))

    ;; A random number between 0 (inclusive) and 1 (exclusive)
    (define (random)
      (/ (random-integer) +interger-size+))

    (define random-real random)

    (define (box-muller)
      (let* ((U1 (random))
             (U2 (random))
             (log-term (sqrt (* -2 (log U1))))
             (angle-term (* 2 +PI+ U2)))
        (cons (* log-term
                 (cos angle-term))
              (* log-term
                 (sin angle-term)))))

    ;; Returns a random number in the normal distribution.
    (define (random-gaussian)
      (car (box-muller)))

    (define (random-pick lst)
      (list-ref lst (random-integer (length lst))))

    ;; Fisher and Yates' modern method
    ;; https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle#Modern_method
    (define (random-shuffle lst)
      (let ((list-swap!
             ;; Swap the element of I-INDEX and J-INDEX in the list LST.
             ;; This procedure does NOT return anything, intsead, it modifies the list LST.
             (lambda (lst i-index j-index)
               (let ((tmp (list-ref lst j-index)))
                 (list-set! lst j-index (list-ref lst i-index))
                 (list-set! lst i-index tmp))))
            (len (length lst))
            (copied-list (list-copy lst)))
        (do ((i (- len 1) (- i 1)))
            ((= i 0)
             copied-list)
          (list-swap! copied-list i (random-integer (+ i 1))))))

    (define (random-true-false)
      (> (random-real) 0.5))

    ;; See `random.scm' for documentation of this procedure.
    (define (random-n-sphere dimension)
      (let* ((samples (collect from 0 to dimension all (random-gaussian)))
             (radius (apply + (map square samples))))
        (map (lambda (x) (/ x radius)) samples)))

    ))

;;;; arc4random.scm ends here.
;;;; thread.scm --- Basic threading library -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This library adds support of multi threading programming.
;; It's JVM-specific library for Kawa Scheme.
;;
;; While `pmap' and `fixed-pmap' look similar to `pmap*', they behave in the radical different way.
;; `pmap*' will run in the background without stopping the flow of the program as long as we don't
;; need the result of `pmap*' yet. Meanwhile, `pmap' and `fixed-pmap*' will run in the frontground
;; but run the given procedure in parallel.

;;; Code:

(define-library (beans thread)
  (import (scheme base)
          (only (kawa base) future))
  (export pmap pmap* fixed-pmap)
  (begin

    ;; Similar to `map' but apply `procedure' to each elment of the lists in parallel.
    ;; This procedure does not *always* create a new thread to handle the procedure. Instead, it will
    ;; create new threads as need but it'll also reuse the old threads if some early-created threads
    ;; finished the tasks before `pmap*' reaches the end of LST or OTHER-LIST.
    ;; Refer to Java's cached threadpool for more details.
    (define (pmap procedure lst . other-list)
      (let* ((exc ::java.util.concurrent.ThreadPoolExecutor
                  (java.util.concurrent.Executors:newCachedThreadPool))
             (result (map (lambda (x::java.util.concurrent.FutureTask) (x:get))
                          (apply map (lambda x (exc:submit (lambda () (apply procedure x))))
                                 lst
                                 other-list))))
        (exc:shutdown)
        result))

    ;; The different from `pmap' is that this procedure will *always* create a new thread to handle
    ;; the PROCEDURE on each elements of LST and OTHER-LIST.
    ;; Creating too much thread can be problematic, so use this procedure with caution.
    (define (pmap* procedure lst . other-list)
      (apply map (lambda x
                   (future (apply procedure x)))
             lst
             other-list))

    ;; Unlike `pmap', this procedure has a fix number of threads that we will reuse them over and over
    ;; to apply PROC to each element of the lists LST and OTHER-LIST.
    ;; The number of threads is 1 less than the number of available processors.
    (define (fixed-pmap procedure lst . other-list)
      (let* ((exc ::java.util.concurrent.ThreadPoolExecutor
                  (java.util.concurrent.Executors:newFixedThreadPool
                   (- ((java.lang.Runtime:getRuntime):availableProcessors) 1)))
             (result (map (lambda (x::java.util.concurrent.FutureTask) (x:get))
                          (apply map (lambda x (exc:submit (lambda () (apply procedure x))))
                                 lst
                                 other-list))))
        (exc:shutdown)
        result))))

;;;; thread.scm ends here.
