;;;; csv.scm --- Working with csv files -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Easy to use csv files manipulator.

;;; Code:

(define-library (beans csv)
  (export read-csv read-csv* write-csv! write-csv*!)
  (import (scheme base)
          (scheme write)
          (scheme file)
          (beans file-utils)
          (beans macro)
          (kawa lib strings_ext))
  (begin

    ;; TODO use lazy reading to process each line and add them into the list which will be returned
    ;; from `read-csv'
    ;; The maximum filesize we can read at one go.
    ;; (define +filesize-limit+ )

    (define (%write-list-to-csv port lst)
      (for-each (lambda (item)
                  (write item port)
                  (display "," port))
                lst))

    ;; sometime, it's better to use `display' instead of `write'
    (define (%write-list-to-csv* port lst)
      (for-each (lambda (item)
                  (display item port)
                  (display "," port))
                lst))

    ;; write a list `lst' to `filename'. `lst' must be a list of lists.
    (define (write-csv! filename lst)
      (call-with-output-file filename
        (lambda (port)
          (for-each (lambda (row)
                      (%write-list-to-csv port row)
                      (newline port))
                    lst))))

    ;; an analog to `write-csv!' which use `display' instead of `write'
    (define (write-csv*! filename lst)
      (call-with-output-file filename
        (lambda (port)
          (for-each (lambda (row)
                      (%write-list-to-csv* port row)
                      (newline port))
                    lst))))

    ;; retrun a list of list of each row of CSV file.
    ;; it's like `read-csv' but it doesn't try to convert each element to a number
    (define (read-csv* filename)
      (->> (readlines filename)
           (map (lambda (row)
                  (string-split row ",")))))

    ;; retrun a list of list of each row of CSV file.
    ;; each row is splitted by "," and each element is then converted to number (if it can)
    (define (read-csv filename)
      (->> (read-csv* filename)
           (map (lambda (row)
                  (map (lambda (element)
                         (let ((converted (string->number (string-trim element))))
                           (if converted
                               converted
                               element)))
                       row)))))))

;;;; csv.scm ends here.
