;;;; regression.scm --- Regression analysis -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:


;;; Code:

(define-library (beans regression)
  (export linear-regression linear-prediction-at-point linear-prediction-from-point)
  (import (scheme base)
          (scheme inexact))
  (begin

    ;; The linear equation is Y = a + bX + error
    ;; This procedure returns, in this order, a, b, standard error of error term, standdard error of a,
    ;; stardard error of b.
    ;; TODO need t-statistics to calculate the confident intervals.
    ;; Data for testing can be found in https://en.wikipedia.org/wiki/Simple_linear_regression
    ;; series x is 1.47 1.50 1.52 1.55 1.57 1.60 1.63 1.65 1.68 1.70 1.73 1.75 1.78 1.80 1.83
    ;; series y is 52.21 53.12 54.48 55.84 57.20 58.57 59.93 61.29 63.11 64.47 66.28 68.10 69.92 72.19 74.46
    (define (linear-regression x-series y-series)
      (let ((lenx (length x-series))
            (leny (length y-series))
            (sum-square (lambda (series) (apply + (map (lambda (x) (square x)) series))))
            (sum-multiple (lambda (x-series y-series)
                            (apply + (map (lambda (x y) (* x y))
                                          x-series
                                          y-series))))
            (sum (lambda (series) (apply + series))))
        (unless (= lenx leny)
          (error "X and Y data range mismatch" linear-regression))
        (let* ((sx (sum x-series))
               (sy (sum y-series))
               (sxx (sum-square x-series))
               (syy (sum-square y-series))
               (sxy (sum-multiple x-series y-series))
               (b (/ (- (* lenx sxy)
                        (* sx sy))
                     (- (* lenx sxx)
                        (square sx))))
               (a (- (/ sy lenx)
                     (* b (/ sx lenx))))
               (variance-error-error-term (/ (- (* lenx syy)
                                                (square sy)
                                                (* (square b)
                                                   (- (* lenx sxx)
                                                      (square sx))))
                                             (* lenx (- lenx 2))))
               (variance-error-b-term (/ (* lenx variance-error-error-term)
                                         (- (* lenx sxx)
                                            (square sx))))
               (variance-error-a-term (/ (* variance-error-b-term sxx)
                                         lenx)))
          (values a b (sqrt variance-error-error-term) (sqrt variance-error-a-term) (sqrt variance-error-b-term)))))

    (define (linear-prediction-at-point a b x)
      (+ a (* b x)))

    ;; from a single point `x', return a list of length `num' that contains values from the prediction
    ;; using `a' and `b', each of which comes from the previous predicted value by this method.
    ;; That is, we give x1, this procedure will return a list of a + bx1, a + b(a + bx1),
    ;; a + b(a + b(a + bx1)), etc.
    ;; It's useful when our model is time serise of the form Xn = a + bXn-1
    (define (linear-prediction-from-point a b x num)
      (let loop ((count 0)
                 (result '()))
        (cond
         ((= count num)
          (reverse result))
         ((null? result)
          (loop (+ count 1)
                (cons (linear-prediction-at-point a b x) result)))
         (else
          (loop (+ count 1)
                (cons (linear-prediction-at-point a b (car result)) result))))))))


;;;; regression.scm ends here.
