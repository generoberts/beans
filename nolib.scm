;;;; nolib.scm --- top level code that don't belong to any library -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This file contains (probably small) top level, quality of life improvement code. Since we may want
;; to use them often, it should not belong to any library. They are "better default" functionalities.
;; of Scheme.

;;; Code:

(define (1+ . nums)
      (+ 1 (apply + nums)))

(define (1- . nums)
  (- (if (= 1 (length nums))
         ;; since (- x) is just -x. If we don't catch this case, we will have (- -x 1) which is
         ;; completely wrong.
         (car nums)
         (apply - nums))
     1))

(define (ignore . data)
  #f)

(define (always . data)
  #t)

(define (print data)
  (display data)
  (newline))

(define (printf format-string . args)
  (apply format #true format-string args))


;;;; nolib.scm ends here.
