;;;; list-util.scm --- mini SRFI-1 (kind of) -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; We implement some functionarities from SRFI-1 and beyond that.

;;; Code:

(define-library (beans list-utils)
  (import (scheme base)
          (scheme cxr)
          (only (kawa base) future))
  (export every some any filter remove remove-if-not sublist enumerate partition combination
          fold fold-left fold-right reduce reduce-left reduce-right unfold unfold-right
          list-comparer list-equal? list-eq? list-eqv? list=
          car+cdr take drop take-right drop-right split-at last drop-last butlast
          zip unzip1 unzip2 unzip3 unzip4 unzip5
          count find find-tail
          take-while drop-while span break list-index
          delete delete-duplicates
          first second third fourth fifth sixth seventh eighth ninth tenth
          list-select composite)
  (begin

    (define (every pred lst)
      (cond
       ((null? lst)
        #true)
       ((pred (car lst))
        (every pred (cdr lst)))
       (else
        #false)))

    (define (some pred lst)
      (cond
       ((null? lst)
        #false)
       ((pred (car lst))
        #true)
       (else
        (some pred (cdr lst)))))

    (define any some)

    ;; take only what's satisfies the `pred'
    (define (filter pred lst)
      (let loop ((lst lst)
                 (result '()))
        (cond
         ((null? lst)
          (reverse result))
         ((pred (car lst))
          (loop (cdr lst)
                (cons (car lst) result)))
         (else
          (loop (cdr lst)
                result)))))

    (define remove-if-not filter)

    (define (remove pred lst)
      (filter (lambda (x) (not (pred x))) lst))

    (define (sublist lst start end)
      (let loop ((result '())
                 (count (- end start)))
        (if (= 0 count)
            result
            (loop (cons (list-ref lst (+ start (- count 1))) result)
                  (- count 1)))))

    (define (%enumerate func lst index)
      (when (not (null? lst))
        (func index (car lst))
        (%enumerate func (cdr lst) (+ index 1))))

    ;; Looping through the list `lst' where supplying both the index and the corresponding element
    ;; to `func'. `func' must be a function that take 2 arguments or more.
    (define (enumerate func lst)
      (%enumerate func lst 0))

    ;; group the list `lst' input a lits of sublists, each with `num' number of elements (except the last one).
    (define (partition num lst)
      (letrec ((%partition
                (lambda (n l r sr) ; num lst result sub-result
                  (cond
                   ((null? l)
                    (reverse (cons (reverse sr) r)))
                   ((= (length sr) n)
                    (%partition n l (cons (reverse sr) r) '()))
                   (else
                    (%partition n (cdr l) r (cons (car l) sr)))))))
        (%partition num lst '() '())))

    ;; https://cookbook.scheme.org/create-k-combinations-from-list/
    ;; Given a number `m' and a list `lst',
    ;; returs a list of all combination of `m' elements from a list `lst'.
    ;; Ex. (combination 2 '(1 2 3)) => ((1 2) (1 3) (2 3))
    ;; Note that this procedure doesn't care about the positon, that is, (1 2) is the same as (2 1)
    (define (%combine n set rest)
      (letrec ((tails-of (lambda (set)
                           (cond
                            ((null? set)
                             '())
                            (else
                             (cons set (tails-of (cdr set)))))))
               (combinations (lambda (n set)
                               (cond
                                ((zero? n)
                                 '())
                                ((= 1 n)
                                 (map list set))
                                (else
                                 (apply append
                                        (map (lambda (tail)
                                               (map (lambda (sub)
                                                      (cons (car tail) sub))
                                                    (combinations (- n 1) (rest tail))))
                                             (tails-of set))))))))
        (combinations n set)))

    (define (combination n set)
      (%combine n set cdr))

    (define (fold f init lst)
      (if (null? lst)
          init
          (fold f (f (car lst) init) (cdr lst))))

    (define fold-left fold)

    (define (fold-right f init lst)
      (if (null? lst)
          init
          (f (car lst) (fold-right f init (cdr lst)))))

    ;; According to SRFI-1
    (define (reduce f init lst)
      (if (null? lst)
          init
          (fold f (car lst) (cdr lst))))

    (define reduce-left reduce)

    (define (reduce-right f init lst)
      (if (null? lst)
          init
          (fold-right f init lst)))

    (define (unfold p f g seed . tail-gen)
      (if (null? tail-gen)
          (unfold p f g seed (lambda (x) '()))
          (if (p seed)
              ((car tail-gen) seed)
              (cons (f seed)
                    (unfold p f g (g seed) (car tail-gen))))))

    (define (unfold-right p f g seed . tail)
      (if (null? tail)
          (unfold-right p f g seed '())
          (let lp ((seed seed)
                   (lis (car tail)))
            (if (p seed)
                lis
                (lp (g seed)
                    (cons (f seed) lis))))))



    ;; compare 2 lists
    ;; This procedure compares lists and
    (define (list-comparer predicate lst1 lst2)
      (cond
       ;; the same length
       ((and (null? lst1)
             (null? lst2)))

       ;; lists of unequal length
       ((or (null? lst1)
            (null? lst2))
        #false)
       (else
        (and (predicate (car lst1) (car lst2))
             (list-comparer predicate (cdr lst1) (cdr lst2))))))

    ;; TODO FIXME
    ;; make themm accepts any number of lists to compare to.
    (define (list-equal? lst1 lst2)
      (list-comparer equal? lst1 lst2))
    (define (list-eq? lst1 lst2)
      (list-comparer eq? lst1 lst2))
    (define (list-eqv? lst1 lst2)
      (list-comparer eqv? lst1 lst2))
    (define (list= lst1 lst2)
      (list-comparer = lst1 lst2))

    (define (car+cdr p)
      (values (car p) (cdr p)))

    ;; return the first `i' elemest of the list `lst'
    (define (take lst i)
      (let loop ((result '())
                 (lst lst)
                 (index i))
        (if (zero? index)
            (reverse result)
            (loop (cons (car lst) result)
                  (cdr lst)
                  (- index 1)))))

    ;; return everything but the first `i' elemest of the list `lst'
    (define (drop lst i)
      (if (zero? i)
          lst
          (drop (cdr lst) (- i 1))))

    (define (take-right lst i)
      (drop lst (- (length lst) i)))

    (define (drop-right lst i)
      (take lst (- (length lst) i)))

    (define (split-at lst i)
      (values (take lst i)
              (drop lst i)))

    (define (last lst)
      (car (reverse lst)))

    (define (drop-last lst)
      (take lst (- (length lst) 1)))

    (define butlast drop-last)

    (define (zip lst . lsts)
      (apply map list lst lsts))

    ;; NOTE don't forget to import `cxr' library
    (define (unzip1 lst)
      (map car lst))
    (define (unzip2 lst)
      (list (unzip1 lst)
            (map cadr lst)))
    (define (unzip3 lst)
      (append (unzip2 lst)
              (map (lambda (x)
                     (car (cdr (cdr x)))) lst)))
    (define (unzip4 lst)
      (append (unzip3 lst)
              (map (lambda (x)
                     (car (cdr (cdr (cdr x))))) lst)))
    (define (unzip5 lst)
      (append (unzip4 lst)
              (map (lambda (x)
                     (car (cdr (cdr (cdr (cdr x)))))) lst)))

    ;; Using `fold' like this is better than using `reduce' as the following
    ;; (reduce + 0 (map (lambda (x) (if (pred x) 1 0))))
    ;; since we don't have to travel through the list `lst' for `map' AND `reduce'
    ;; That is, it's O(n) instead of O(n**2)
    (define (count pred lst)
      (fold (lambda (x count)
              (if (pred x)
                  (+ count 1)
                  count))
            0
            lst))

    ;; similar to `some' but instead of boolean value, it returns the value it found.
    (define (find pred lst)
      ;; code reuse from `some', only change it to make it returs `(car lst)'
      (cond
       ((null? lst)
        #false)
       ((pred (car lst))
        (car lst))
       (else
        (find pred (cdr lst)))))

    ;; similar to `find' but return the whole remaining elements of the list.
    (define (find-tail pred lst)
      ;; Again, code reuse from `some' with a tiny change.
      ;; TODO genelarize this.
      (cond
       ((null? lst)
        #false)
       ((pred (car lst))
        lst)
       (else
        (find-tail pred (cdr lst)))))

    (define (take-while pred lst)
      (if (not (pred (car lst)))
          '()
          (cons (car lst) (take-while pred (cdr lst)))))

    (define (drop-while pred lst)
      (if (pred (car lst))
          (drop-while pred (cdr lst))
          lst))

    (define (span pred lst)
      (values (take-while pred lst)
              (drop-while pred lst)))

    (define (break pred lst)
      (span (lambda (x) (not (pred x))) lst))

    (define (list-index pred . lst)
      (let loop ((index 0)
                 (all-lst lst))
        (cond
         ((some null? all-lst)
          #f)
         ((apply pred (map car all-lst))
          index)
         (else
          (loop (+ index 1)
                (map cdr all-lst))))))

    (define (delete x lst . equal)
      (if (null? equal)
          (delete x lst equal?)
          (let loop ((result '())
                     (lst lst)
                     (pred (car equal)))
            (cond
             ((null? lst)
              (reverse result))
             ((pred x (car lst))
              (loop result
                    (cdr lst)
                    pred))
             (else
              (loop (cons (car lst) result)
                    (cdr lst)
                    pred))))))

    (define (delete-duplicates lst . equal)
      (if (null? equal)
          (delete-duplicates lst equal?)
          (let loop ((acc '())
                     (lst lst)
                     (pred (car equal)))
            (cond
             ((null? lst)
              (reverse acc))
             ((member (car lst) acc pred)
              (loop acc
                    (cdr lst)
                    pred))
             (else
              (loop (cons (car lst) acc)
                    (cdr lst)
                    pred))))))

    (define first car)
    (define second cadr)
    (define third caddr)
    (define fourth cadddr)
    (define (fifth x)
      (car (cddddr x)))
    (define (sixth x)
      (cadr (cddddr x)))
    (define (seventh x)
      (caddr (cddddr x)))
    (define (eighth x)
      (cadddr (cddddr x)))
    (define (ninth x)
      (car (cddddr (cddddr x))))
    (define (tenth x)
      (cadr (cddddr (cddddr x))))

    ;; select elements of `lst' from `indexs'
    (define (list-select lst indexes)
      (if (null? indexes)
          '()
          (cons (list-ref lst (car indexes))
                (list-select lst (cdr indexes)))))

    ;; https://stackoverflow.com/questions/32782508/scheme-function-that-return-composition-of-functions
    ;; (composite f g h) => (lambda (arg) (f (g (h (args))))) and so on
    (define (composite . procs)
      (define (comp-rec arg)
        (if (null? procs)
            arg
            (let ((proc (car procs))
                  (rest (cdr procs)))
              (set! procs rest)
              (proc (comp-rec arg)))))
      comp-rec)
    ))

;;;; list-util.scm ends here.
