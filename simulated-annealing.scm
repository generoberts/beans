;;;; simulated-annealing.scm -- Implementation of Simulate Annealing

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;; https://en.wikipedia.org/wiki/Simulated_annealing
;;; https://rosettacode.org/wiki/Simulated_annealing

;;; Code:

(define-library (beans simulated-annealing)
  (import (scheme base)
          (scheme inexact) ; exp
          (beans random)
          (beans macro)
          (beans list-utils))
  (export simulated-annealing)
  (begin

    (define (probability deltaE T)
      (if (< deltaE 0)
          1
          (exp (- (/ deltaE T)))))

    ;; `kmax' is the number of iteration we want to process.
    ;; `init-state' is the initial state of the sytem.
    ;; `temperature-function' is a function of 2 arguments. It takes the current iteration count and
    ;; the number of max iteration (kmax), and returns the number represents the current temperature.
    ;; Typically, it be `(lambda (k kmax) (- 1 (/ k kmax)))'.
    ;; `energy-function' Energy that we want to minimize i.e. objective function. It is a function that
    ;; takes the current state of each iteration, and returns a number represents the system's energy.
    ;; `next-state-function' is a fuction that determine the next state from the current state. It takes
    ;; the current state and returns a new state.
    (define (simulated-annealing kmax
                                 init-state
                                 temperature-function
                                 energy-function
                                 next-state-function)
      (let ((state init-state)
            (old-energy #false))
        (dotimes (k kmax)
          (let* ((temp (temperature-function k kmax))
                 (next-state (next-state-function state))
                 (next-state-energy (energy-function next-state))
                 (deltaE (- next-state-energy
                            (if old-energy
                                old-energy
                                (energy-function state)))))
            (when (>= (probability deltaE temp)
                      (random-real))
              (set! state next-state)
              (set! old-energy next-state-energy))))
        state))

    ;; Example call of `simulated-annealing' to find minima of Himmelblau's function.
    ;; (simulated-annealing 100000
    ;;                      '(0 0)
    ;;                      (lambda (k kmax)
    ;;                        (- 1 (/ k kmax)))
    ;;                      (lambda (coordinate)
    ;;                        (let ((x (first coordinate))
    ;;                              (y (second coordinate)))
    ;;                          (+ (square (- (+ (square x)
    ;;                                           y)
    ;;                                        11))
    ;;                             (square (- (+ x
    ;;                                           (square y))
    ;;                                        7)))))
    ;;                      (lambda (coordinate)
    ;;                        ;; Himmelblau's function
    ;;                        ;; limit the coordinate to the bound of Booth function (10x10 size in)
    ;;                        (let ((new-x (+ (random-gaussian)
    ;;                                        (first coordinate)))
    ;;                              (new-y (+ (random-gaussian)
    ;;                                        (second coordinate))))
    ;;                          (list (if (> (abs new-x) 5)
    ;;                                    (* 2 (random-gaussian))
    ;;                                    new-x)
    ;;                                (if (> (abs new-y) 5)
    ;;                                    (* 2 (random-gaussian))
    ;;                                    new-y)))))
    ))

;;;; simulated-annealing.scm ends here.
