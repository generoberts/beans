;;;; macro.scm --- little macros -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Chicken Scheme is also has a lot of useful macro as well, we can find them at
;; http://wiki.call-cc.org/eggref/5/miscmacros
;; Even if we don't look at its code but it inspired us and it doesn't hurt to put their license in
;; `LICENSE-others' file so let's do it.

;; Chicken Scheme's Clojurian implements some interesting and useful macos from Clojure. We can find
;; its source code at https://bitbucket.org/DerGuteMoritz/clojurian/src/master/

;; Chicken Scheme's Anaphora egg implemented anaphorics macros. We take some of them which can
;; be implemented in pure R7Rs (small) Scheme. We take them from the version of "Last update: Feb 25, 2020".
;; See `LICENSE-others' file for its licenes.

;;; Code:

(define-library (beans macro)
  (import (scheme base)
          (scheme time))
  (export if-not if-let while until when-not when-let ensure
          push! pop! inc! dec!
          dotimes dolist repeat
          -> ->> if-let*
          nif nwhen ncond nwhile nand nlambda
          collect
          time)
  (begin

    (define-syntax if-not
      (syntax-rules ()
        ((if-not test then else)
         (if (not test)
             then
             else))))

    ;; evaluate `test' and bind it to `bind', if `bind' is true, then it evaluate `then', otherwise
    ;; evaluate `else'.
    (define-syntax if-let
      (syntax-rules ()
        ((if-let (bind test) then else)
         (let ((bind test))
           (if bind then else)))))

    ;; keep evaluating its body until the `test' is false.
    (define-syntax while
      (syntax-rules ()
        ((while test command ...)
         (do ((i 0 0))
             ((not test))
           (begin
             command ...)))))

    ;; like `while', but until the `test' is true.
    (define-syntax until
      (syntax-rules ()
        ((until test command ...)
         (while (not test)
                command ...))))

    (define-syntax when-not
      (syntax-rules ()
        ((when-not test command ...)
         (when (not test)
           command ...))))

    ;; evaluate `test' and bind it to `bind', if `bind' is true then it evaluates its body in the
    ;; given order.
    (define-syntax when-let
      (syntax-rules ()
        ((when-let (bind test) command ...)
         (let ((bind test))
           (when bind
             command ...)))))

    ;; evaluate `exp' and apply `predicate' to the result. If the result is true, then it returns
    ;; the result, else it signals an error.
    (define-syntax ensure
      (syntax-rules ()
        ((ensure predicate exp)
         (let ((result exp))
           (if (predicate result)
               result
               (error "Expression doesn't satisfy the predicate" ensure))))))

    ;; append `x' into a list `lst'. If `x' isn't a list, wrap it in a list and append it.
    (define-syntax push!
      (syntax-rules ()
        ((push! x lst)
         (set! lst (append lst (if (list? x) x (list x)))))))

    ;; returns the first item of the list `lst' and set the `lst' as the rest of the remaining items.
    (define-syntax pop!
      (syntax-rules ()
        ((pop! lst)
         (let ((first-element (car lst)))
           (set! lst (cdr lst))
           first-element))))

    ;; increase the given number
    (define-syntax inc!
      (syntax-rules ()
        ((inc! num)
         (set! num (+ num 1)))
        ((inc! num amount)
         (set! num (+ num amount)))))

    ;; decrease the given number
    (define-syntax dec!
      (syntax-rules ()
        ((dec! num)
         (set! num (- num 1)))
        ((dec! num amount)
         (set! num (- num amount)))))

    ;; cons-ing elements
    (define-syntax cons!
      (syntax-rules ()
        ((cons! val1 val2)
         (set! val1 (cons val1 val2)))))

    ;; append-ing `val' into `lst'
    (define-syntax append!
      (syntax-rules ()
        ((append! lst val)
         (set! lst (append lst val)))))

    ;; looping from 0 to times-1
    (define-syntax dotimes
      (syntax-rules ()
        ((dotimes (i times) body ...)
         (do ((i 0 (+ i 1)))
             ((= i times))
           body
           ...))))

    ;; not quit the same as Common Lisp one, isn't it?
    ;; looping over a list.
    ;; TODO should we support mulitple list?
    (define-syntax dolist
      (syntax-rules ()
        ((dolist (item lst) body ...)
         (for-each (lambda (item)
                     body
                     ...)
                   lst))))

    ;; keep evaluating its body for `times' times.
    (define-syntax repeat
      (syntax-rules ()
        ((repeat times command ...)
         (dotimes (i times)
                  command ...))))

    ;;; These are from Chicken Scheme's Clojurian egg (https://bitbucket.org/DerGuteMoritz/clojurian/src/master/)
    ;; originally contributed by Martin DeMello
    ;; rewritten in terms of syntax-rules by Moritz Heidkamp
    (define-syntax ->
      (syntax-rules ()
        ((-> x) x)
        ((-> x (y z ...) rest ...)
         (-> (y x z ...) rest ...))
        ((-> x y rest ...)
         (-> x (y) rest ...))))

    (define-syntax ->>
      (syntax-rules ()
        ((->> x) x)
        ((->> x (y ...) rest ...)
         (->> (y ... x) rest ...))
        ((->> x y rest ...)
         (->> x (y) rest ...))))

    (define-syntax if-let*
      (syntax-rules ()
        ((if-let* ((x y) more ...) then else)
         (car (or (and-let* ((x y) more ...)
                            (list then))
                  (list else))))))

    ;;; Anaphoric macros
    ;; R7Rs (small) Scheme can't implement `true' anaphoric macro, can it? That requires `syntax-case' which
    ;; isn't available on R7Rs (small).
    ;; Instead, we end up with `named' anaphoric macros instead.
    (define-syntax nif
      (syntax-rules ()
        ((nif name test consequent)
         (let ((name test))
           (if name consequent)))
        ((nif name test consequent alternative)
         (let ((name test))
           (if name consequent alternative)))))

    (define-syntax nwhen
      (syntax-rules ()
        ((nwhen name test xpr . xprs)
         (let ((name test))
           (if name (begin xpr . xprs))))))

    (define-syntax ncond
      (syntax-rules (else)
        ((ncond name) #f)
        ((ncond name (else xpr . xprs) . clauses)
         (let ((sym #t))
           (if sym
               (let ((name sym)) xpr . xprs)
               #f)))
        ((ncond name (test xpr . xprs) . clauses)
         (let ((sym test))
           (if sym
               (let ((name sym)) xpr . xprs)
               (ncond name . clauses))))))

    (define-syntax nwhile
      (syntax-rules ()
        ((nwhile name test xpr . xprs)
         (let loop ((name test))
           (when name
             (begin xpr . xprs)
             (loop test))))))

    (define-syntax nand
      (syntax-rules ()
        ((nand name) #t)
        ((nand name arg) arg)
        ((nand name arg0 arg1 ...)
         (let ((name arg0))
           (if name (nand name arg1 ...))))))

    (define-syntax nlambda
      (syntax-rules ()
        ((nlambda name args xpr . xprs)
         (letrec ((name (lambda args xpr . xprs)))
           name))))

    ;; Examples.
    ;; (nmap them (+ (square (car them)) (square (cadr them)) (square (caddr them))) '(1 2 3) '(2 4 6) '(3 5 7))
    ;; => (14 45 94)
    ;; (nmap them (+ (square (car them)) (square (cadr them)) (square (caddr them))) '(1) '(2 4 6) '(3 5 7))
    ;; => (14)
    ;; (nmap it (square it) '(1 2 3))
    ;; => (1 4 9)
    (define-syntax nmap
      (syntax-rules ()
        ((nmap name (head . body) lst)
         (map (lambda (name) (head . body)) lst))
        ((nmap name (head . body) lst . lists)
         (apply map (lambda name (head . body)) lst (list . lists)))))

    ;; Simple Loop-Related Macros.
    ;; You can call this macro with or without an argument for each value in this list this macro
    ;; collects. That is, you can call it either `(collect from 0 to 10)' or
    ;; `(collect from 0 to 10 each (lambda (x) (do-something-with x)))'.
    ;;
    ;; If you don't specify the argument you can pass a function that doesn't accept any argument to
    ;; the macro call, each argument in the result list will be the result of that function call.
    ;; For example, let's say a function all like (random) returns a random number, we can call this
    ;; `collect' macro with `(collect from 0 to 10 all (random))' and has a list of 10 random numbers.
    ;;
    ;; If you specify the argument to the macro, you can pass a function that accepts 1 argument to
    ;; the macro. The macro will call that function to its "indexes" when it builds the result list.
    ;; For example, `(collect from 0 to 10 each (lambda (x) (* 10 x)))' returns
    ;; (0 10 20 30 40 50 60 70 80 90) since each index, from 0 to _less than_ 10 i.e. 9, is applied to
    ;; the function `(lambda (x) (* 10 x))'.
    (define-syntax collect
      (syntax-rules (from to each)
        ((collect from this to that each func)
         (do ((i (- that 1) (- i 1))
              (result '() (cons (func i) result)))
             ((> this i)
              result)))
        ((collect from this to that all val)
         (collect from this to that each (lambda (x) val)))
        ((collect from this to that)
         (do ((i (- that 1) (- i 1))
              (result '() (cons i result)))
             ((> this i)
              result)))))

    ;; Returns a number of seconds to run the body of the macro.
    (define-syntax time
      (syntax-rules ()
        ((time head . body)
         (let ((current (current-jiffy)))
           (begin head . body)
           (inexact (/ (- (current-jiffy) current) (jiffies-per-second)))))))))

;;;; macro.scm ends here.
