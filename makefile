CLASSES = random.class list-utils.class math.class sort.class stats.class regression.class nums.class file-utils.class graph.class  macro.class  matrix.class string.class robot.class csv.class patmat.class

TEST_CASES = matrix-test.scm nums-test.scm math-test.scm patmat-test.scm

COMPLETE_FILES = nolib.scm math.scm list-utils.scm macro.scm random.scm sort.scm stats.scm nums.scm file-utils.scm graph.scm matrix.scm string.scm robot.scm csv.scm regression.scm patmat.scm debug.scm fft.scm arc4random.scm thread.scm

all: $(CLASSES)

%.class: %.scm
	kawa -C $<

test:
	for f in $(TEST_CASES); do \
		kawa -f $$f; \
	done

clean:
	rm -f beans/*.class && rm -f beans/*/*.class

merge:
	cat $(COMPLETE_FILES) > kawarc.scm

	echo ";;;; Version v0.8" | cat - kawarc.scm > tmp  && mv tmp kawarc.scm

	cp -u ./kawarc.scm ~/.kawarc.scm

.PONY:
	clear
