;;;; mouse-location.scm --- getting location of the mouse -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This is a simple programe to use with `robot' library to tell the robot where to preform mouse actions.

;;; Code:

(define robot ::java.awt.Robot (make java.awt.Robot))

(define (mouse-location)
  (do ((i 0 0))
      (#false) ; infinity loop
    (let ((x (((java.awt.MouseInfo:getPointerInfo):getLocation):getX))
          (y (((java.awt.MouseInfo:getPointerInfo):getLocation):getY)))
      (display x)
      (display ":")
      (display y)
      (newline))))

(mouse-location)

;;;; mouse-location.scm ends here.
