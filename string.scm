;;;; string.scm --- utilities for strings -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Originally, I wanted a procedure to concate strings but consider that R7Rs (small) provides only a
;; few numbers of procedure for strings, so it's better to create my own library for these kind of tracks.


;;; Code:

(define-library (beans string)
  (export concate string-replace)
  (import (scheme base))
  (begin

    (define concate string-append)

    ;; Replacet the OLD string with the NEW string in the STR string.
    ;; Returns the new string.
    (define (string-replace str::String old new)
      (str:replaceAll old new))))


;;;; string.scm ends here.
