;;;; file-utils.scm --- Common tasks that are done with files -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Here we extend the common procedures that are suitable for day-to-day tasks.

;;; Code:


(define-library (beans file-utils)
  (export readlines for-each-line write-to-file
          load-scheme-object save-scheme-object
          with-lazy-readlines)
  (import (scheme base)
          (scheme file)
          (scheme write)
          (scheme read))
  (begin

    ;; reading a file line-by-line, each line is an element in a list
    ;; By user "Will" from https://stackoverflow.com/questions/16335454/reading-from-file-using-scheme
    (define (readlines file-name)
      (call-with-input-file file-name
        (lambda (port)
          (let loop ((line (read-line port))
                     (result '()))
            (if (eof-object? line)
                (reverse result)
                (loop (read-line port) (cons line result)))))))

    ;; similar to `readlines' but in case we want to process file line-by-line without storing it in
    ;; a variable.
    ;; PROC must a procedure that accepts one argument.
    (define (for-each-line file-name proc)
      (let ((all-line (readlines file-name)))
        (for-each (lambda (line) (proc line))
                  all-line)))

    ;; writing each elment of the given list to a file
    (define (write-to-file file-name lst)
      (call-with-output-file file-name
        (lambda (port)
          (for-each (lambda (element)
                      (write element port)
                      (newline port))
                    lst))))

    ;; saving `obj' to a file to use later.
    ;; TODO already test with a list and vector, need more test. But isn't
    ;; R7Rs guarantee usage of `write' and `read' like this?
    (define (save-scheme-object file-name obj)
      (call-with-output-file file-name
        (lambda (port) (write obj port))))

    ;; usage: (define x (load-scheme-object "file-name"))
    (define (load-scheme-object file-name)
      (call-with-input-file file-name
        (lambda (port) (read port))))

    ;; reading a file `filename' line by line, each line is process using `proc' which must be a
    ;; procedure that accepts one argument.
    ;; It's good when we have to read and process each line of a large file.
    (define (with-lazy-readlines filename proc)
      (let* ((%file ::java.io.FileInputStream (java.io.FileInputStream (->java.lang.String filename)))
             (%sc ::java.util.Scanner (java.util.Scanner %file)))
        (dynamic-wind
            (lambda () #f)
            (lambda ()
              (do ((i #t)) ;; infinite loop
                  ((not (%sc:hasNextLine)))
                (proc (%sc:nextLine))))
            (lambda ()
              (%file:close)
              (%sc:close)))))))

;;;; file-utils.scm ends here.
