;;;; ols.scm --- Ordinary Least Squares -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Ordinary Least Squares is one of several methods to estimate the unknown parameters in
;; linear regresssion model.

;;; Code:

(define-library (beans math regression linear ols)
  (import (scheme base)
          (prefix (beans math matrix) mm-))
  (export )
  (begin
    ))

;;;; ols.scm ends here.
