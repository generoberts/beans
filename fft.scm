;;;; fft.scm --- Fast Fourier Transfrom -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This is an implementation of Cooley-Tukey's Fast Fourier Transformations algorithm.

;;; Code:

(define-library (beans math fft)
  (import (scheme base)
          (scheme inexact)
          (scheme complex)
          (beans macro)
          (beans list-utils)
          (beans math))
  (export fft fft->mag fft->magnitude fft->angle fft->degree fft->real fft->imag)
  (begin

    (define (spit-even-odd-terms x)
      (let loop ((even-terms '())
                 (odd-terms '())
                 (input x)
                 (even-turn? #true)) ; the index 0 is even index
        (cond
         ((null? input)
          (values (reverse even-terms)
                  (reverse odd-terms)))
         (even-turn?
          (loop (cons (car input) even-terms)
                odd-terms
                (cdr input)
                #false))
         (else
          (loop even-terms
                (cons (car input) odd-terms)
                (cdr input)
                #true)))))

    ;; pad the list with 0s to the length of power of 2
    (define (padding-list lst)
      (let* ((current-length (length lst))
             ;; the smallest power of two that is less than current length
             (log2-of-length (log current-length 2)))
        ;; it's already a power of 2, so just return the list `lst'
        (if (integer? log2-of-length)
            lst
            (let* ((power-of-two-before (exact (floor log2-of-length)))
                   (next-power-of-two (+ 1 power-of-two-before))
                   ;; how many more we have to pad to get to the next power of two
                   (pad-length (- (expt 2 next-power-of-two) current-length)))
              (append lst
                      (collect from 0 to pad-length all 0))))))

    ;; NOTE this procedures that the length of `x' is a power of 2.
    (define (%fft x)
      (let ((N (length x)))
        (if (= N 1)
            x
            (let-values (((even-terms-raw odd-terms-raw)
                          (spit-even-odd-terms x)))
              (let* ((even-terms (%fft even-terms-raw))
                     (odd-terms (%fft odd-terms-raw))
                     (T (collect from 0 to (/ N 2)
                                 each (lambda (k)
                                        (* (expt +E+ (/ (* -2i +PI+ k) N))
                                           (list-ref odd-terms k))))))
                (append (collect from 0 to (/ N 2)
                                 each (lambda (k)
                                        (+ (list-ref even-terms k)
                                           (list-ref T k))))
                        (collect from 0 to (/ N 2)
                                 each (lambda (k)
                                        (- (list-ref even-terms k)
                                           (list-ref T k))))))))))

    (define (fft x)
      (%fft (padding-list x)))

    (define (fft->mag x)
      (map magnitude (fft x)))

    (define fft->magnitude fft->mag)

    (define (fft->angle x)
      (map angle (fft x)))

    ;; "Normal" angle that a circle has 360 degree.
    ;; We don't map over `fft->angle' again because we don't want to travel all the way over the
    ;; result list again.
    (define (fft->degree x)
      (map (lambda (x)
             (/ (* (angle x) 360) (* 2 +PI+)))
           (fft x)))

    (define (fft->real x)
      (map real-part (fft x)))

    (define (fft->imag x)
      (map imag-part (fft x)))))


;;;; fft.scm ends here.
