;;;; nums.scm --- Numpy clone -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This is a partial port of Numpy (Python library.)
;; Numpy is licensed under BSD-3-Clause.
;; Even if we said this is a `port', it's actually more like a mimic of Numpy.
;; The source of this library's behaviors is is from the Numpy's doc (https://numpy.org/doc/stable/)

;;; Code:

(define-library (beans nums)
  (import (scheme base)
          (scheme cxr)
          (beans random)
          (beans list-utils)
          (beans math stats))
  (export ndarray? arange linspace ndim shape size reshape
          zeros ones random-array ndarray-operate array-plus array-minus array-mul array-div
          ndarray-operate-unary matrix-multipliable? matrix-mul
          ndarray->list ravel transpose
          ndarray-groupping-operate array-sum array-cumsum array-min array-max
          select slice hstack vstack row-stack column-stack)
  (begin

    ;; An ndarray is a list of lists
    (define (ndarray? array)
      (and (list? array)
           (every list? array)))

    ;; if given a number, returns  a list of numbers from 0 to n - 1.
    ;; if given a list of 3 elements, the first element is starting number, the second is the ending
    ;; number, and the third is the step size.
    (define (arange n)
      (cond
       ((and (integer? n)
             (or (positive? n)
                 (zero? n)))
        (let loop ((result '())
                   (count 0))
          (if (= count n)
              (reverse result)
              (loop (cons count result)
                    (+ count 1)))))
       ((and (list? n)
             (= 3 (length n)))
        (let ((start-num (car n))
              (end-num (cadr n))
              (step (caddr n)))
          (let loop ((result '())
                     (count 0))
            (let ((result-this-iter (+ start-num (* count step))))
              (if (>= result-this-iter end-num)
                  (reverse result)
                  (loop (cons result-this-iter result)
                        (+ count 1)))))))
       (else
        (error "unknow argument" arange))))

    ;; Similar to `arange' but `arange' can't control how long the result ndarray will be.
    ;; This procedure will can be used when we want to control our result's length.
    ;; We use `(+ end step)' in the last expression since `arange' doesn't included the ending number in its result.
    ;; So we have to manually add it like that.
    (define (linspace start end len)
      (let* ((how-long (- end start))
             (step (/ how-long (- len 1))))
        (list (arange (list start (+ end step) step)))))

    (define (ndim array)
      (if (not (list? array))
          0
          (+ 1 (ndim (car array)))))

    (define (shape array)
      (cond
       ((not (list? array))
        '())
       (else
        (cons (length array) (shape (car array))))))

    (define (size array)
      (apply * (shape array)))

    ;; partion a list into a list of lists, each with `size' elements
    (define (%partition lst size)
      (let loop ((result '())
                 (current-list '())
                 (the-list lst))
        (cond
         ((and (null? the-list)
               (null? current-list))
          (reverse result))
         ((= (length current-list) size)
          (loop (cons (reverse current-list) result)
                '()
                the-list))
         (else
          (loop result
                (cons (car the-list) current-list)
                (cdr the-list))))))

    ;; from a ndarray, turns it into regular list
    (define (%flatten lst)
          (cond
           ((null? lst) '())
           ((pair? (car lst))
            (append (%flatten (car lst))
                    (%flatten (cdr lst))))
           (else
            (cons (car lst) (%flatten (cdr lst))))))

    (define (reshape array dim)
      (cond
       ;; turning any ndarray into an array of 1 dimension.
       ;; we have to wrap it in `list' since we want a ndarray as an output.
       ((and (ndarray? array)
             (= 1 (size dim)))
        (list (%flatten array)))
       ;; basically reshaping of a matrix
       ((and (ndarray? array)
             (= 2 (size dim)))
        (if (or (= -1 (cadr dim))
                (= (size array)
                   (apply * dim)))
            (let* ((flatten-array (%flatten array))
                   (result-size (/ (size array) (car dim)))
                   (result (%partition flatten-array result-size)))
              result)
            (error "Invalided size" reshape)))
      ((> (size dim) 2)
        (error "Unimplemented dimension" reshape))
       (else
        (error "Unknow error" reshape))))

    ;; like `arange' but we can specific each element by `element' which is a procedure that accepts
    ;; no arguments.
    ;; this is helpful for implementing `zeros', `ones', and list of random numbers.
    (define (%arange-specific element-gen n)
      (let loop ((result '())
                 (count 0))
        (if (= count n)
            (list (reverse result))
            (loop (cons (element-gen) result)
                  (+ count 1)))))

    (define (zeros n)
      (cond
       ((integer? n)
        (%arange-specific (lambda () 0) n))
       ((and (list? n)
             (= 2 (length n)))
        (let ((len (apply * n)))
          (reshape (%arange-specific (lambda () 0) len) n)))
       (else
        (error "invalide argument" zeros))))

    (define (ones n)
      (cond
       ((integer? n)
        (%arange-specific (lambda () 1) n))
       ((and (list? n)
             (= 2 (length n)))
        (let ((len (apply * n)))
          (reshape (%arange-specific (lambda () 1) len) n)))
       (else
        (error "invalide argument" ones))))

    (define (random-array n)
      (cond
       ((integer? n)
        (%arange-specific (lambda () (random)) n))
       ((and (list? n)
             (= 2 (length n)))
        (let ((len (apply * n)))
          (reshape (%arange-specific (lambda () (random)) len) n)))
       (else
        (error "invalide argument" random-array))))

    ;; binary operation on two arrays.
    ;; TODO this is implemented only on maximum of 2 dimesion ndarray, i.e. a matrix.
    (define (ndarray-operate pred array1 array2)
      (if (not (= (size array1)
                  (size array2)))
          (error "Can't operate on arrays of difference size" ndarray-operate)
          (map (lambda (x y)
                 (map (lambda (x-sub y-sub)
                        (pred x-sub y-sub))
                      x y))
               array1 array2)))

    ;; note that `array-mul' is element-wise multiplication.
    ;; For matrix multiplication, use `matrix-mul'.
    (define (array-plus a1 a2)
      (ndarray-operate + a1 a2))
    (define (array-minus a1 a2)
      (ndarray-operate - a1 a2))
    (define (array-mul a1 a2)
      (ndarray-operate * a1 a2))
    (define (array-div a1 a2)
      (ndarray-operate / a1 a2))

    ;; unary operation on an array.
    ;; TODO this is implemented only on maximum of 2 dimesion ndarray, i.e. a matrix.
    (define (ndarray-operate-unary pred array)
      (map (lambda (x)
             (map (lambda (x-sub)
                    (pred x-sub))
                  x))
           array))

    (define (matrix-multipliable? m1 m2)
      (= (cadr (shape m1))
         (car (shape m2))))

    (define (matrix-mul a1 a2)
      (if (not (matrix-multipliable? a1 a2))
          (error "Rank mismatch" matrix-multipliable?)
          (map
           (lambda (row)
             (apply map
                    (lambda column
                      (apply + (map * row column)))
                    a2))
           a1)))

    ;; from a array, to a plain list
    (define ndarray->list %flatten)

    ;; similar to `ndarray->list' but this returns a ndarray instead of list
    (define (ravel array)
      (list (ndarray->list array)))

    (define (transpose mtr)
      (apply map list mtr))

    ;; instead of operating on each element, we can operate on all of the element, or from each row
    ;; or from each column.
    ;; The valide values for axis are 'all, 'row, and 'column.
    (define (ndarray-groupping-operate array axis pred)
      (case axis
        ;; This is when we treat an array as a big list.
        ;; We wrap the result in `list' because we want the result to be `ndarray', not a list.
        ((all)
         (list (apply pred (ndarray->list array))))
        ((col)
         (list (map (lambda (row) (apply pred row)) array)))
        ((row)
         (list (map (lambda (col) (apply pred col)) (transpose array))))))

    (define (array-sum array axis)
      (ndarray-groupping-operate array axis +))

    (define (array-cumsum array axis)
      (ndarray-groupping-operate array axis cumsum))

    (define (array-min array axis)
      (ndarray-groupping-operate array axis min))

    (define (array-max array axis)
      (ndarray-groupping-operate array axis max))

    (define (select array row col)
      (list-ref (list-ref array row) col))

    ;; axis is 'row or 'col.
    ;; This procedure selecet only the start-th element to the (end-1)-th element of row/column axis
    ;; from the array.
    ;; we use `car' on the result because it somehow wraps the result in another list.
    ;; If it's 1darray, give `axis' an 'all.
    (define (slice array axis start end )
      (ndarray-groupping-operate array axis (lambda x (sublist x start end))))

    (define vstack append)

    (define (hstack a1 a2)
      (map (lambda (row1 row2) (append row1 row2)) a1 a2))

    ;; stack 2 1darraies into 2darray.
    ;; if a1 and a2 are already 2darray, use `hstack'.
    (define (column-stack a1 a2)
      (if (and (= 1 (car (shape a1)))
               (= 1 (car (shape a2))))
          (append a1 a2)
          (hstack a1 a2)))

    (define row-stack vstack)

    ))


;;;; nums.scm ends here.
