;;;; thread.scm --- Basic threading library -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This library adds support of multi threading programming.
;; It's JVM-specific library for Kawa Scheme.
;;
;; While `pmap' and `fixed-pmap' look similar to `pmap*', they behave in the radical different way.
;; `pmap*' will run in the background without stopping the flow of the program as long as we don't
;; need the result of `pmap*' yet. Meanwhile, `pmap' and `fixed-pmap*' will run in the frontground
;; but run the given procedure in parallel.

;;; Code:

(define-library (beans thread)
  (import (scheme base)
          (only (kawa base) future))
  (export pmap pmap* fixed-pmap)
  (begin

    ;; Similar to `map' but apply `procedure' to each elment of the lists in parallel.
    ;; This procedure does not *always* create a new thread to handle the procedure. Instead, it will
    ;; create new threads as need but it'll also reuse the old threads if some early-created threads
    ;; finished the tasks before `pmap*' reaches the end of LST or OTHER-LIST.
    ;; Refer to Java's cached threadpool for more details.
    (define (pmap procedure lst . other-list)
      (let* ((exc ::java.util.concurrent.ThreadPoolExecutor
                  (java.util.concurrent.Executors:newCachedThreadPool))
             (result (map (lambda (x::java.util.concurrent.FutureTask) (x:get))
                          (apply map (lambda x (exc:submit (lambda () (apply procedure x))))
                                 lst
                                 other-list))))
        (exc:shutdown)
        result))

    ;; The different from `pmap' is that this procedure will *always* create a new thread to handle
    ;; the PROCEDURE on each elements of LST and OTHER-LIST.
    ;; Creating too much thread can be problematic, so use this procedure with caution.
    (define (pmap* procedure lst . other-list)
      (apply map (lambda x
                   (future (apply procedure x)))
             lst
             other-list))

    ;; Unlike `pmap', this procedure has a fix number of threads that we will reuse them over and over
    ;; to apply PROC to each element of the lists LST and OTHER-LIST.
    ;; The number of threads is 1 less than the number of available processors.
    (define (fixed-pmap procedure lst . other-list)
      (let* ((exc ::java.util.concurrent.ThreadPoolExecutor
                  (java.util.concurrent.Executors:newFixedThreadPool
                   (- ((java.lang.Runtime:getRuntime):availableProcessors) 1)))
             (result (map (lambda (x::java.util.concurrent.FutureTask) (x:get))
                          (apply map (lambda x (exc:submit (lambda () (apply procedure x))))
                                 lst
                                 other-list))))
        (exc:shutdown)
        result))))

;;;; thread.scm ends here.
