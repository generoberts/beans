;;;; patmat.scm --- Patter Matching -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Basic patter matching support.

;; This patter matching has 2 type of matching supported; single value matching and list matching.
;; For either of them, `otherwise' case will always get matched, and using underscore, _, as a
;; wildcard matching. We can also use custom predicate function. A custom predicate function accepts
;; 2 argument, and will use the value we want to match with as the first argument while using the
;; case's value as the second argument to the custom predicate function.

;; There are 2 type of matching; a single value matching and a list matching.
;; A single value matching does _not_ wrap its cases into lists.
;; (match x
;;       (1 'some-thing) ; match if and only if x is 1
;;       (_ 'other-thing) ; match with any value of x
;;       (otherwise 'last-thing)) ; match with any vaule of x, like using _
;; To use a custome comparator, use `using' keyword after the value you want to match.
;; (match x using my-function
;;       (1 'some-thing) ; match if and only if (my-func x 1) is true
;;       (_ 'other-thing) ; always match
;;       (otherwise 'last-thing)) ; always match as well.
;; The above examples use a number, but we can use a symbol as well. Though that will generate an
;; warning message but it'll work fine.

;; A list matching wraps each case in a list, and you can use _ to skip the element you don't care
;; about. The value you want to match with must be a list.
;; (match my-list
;;        ((_ 2 3) 'some-thing) ; `my-list' must have 2 and 3 as its 2nd and 3rd elements
;;        ((1 _ 3) 'some-other-thing) ; _ can be used in the middle
;;        ((1 2 3) 'more-thing) ; `my-list' must be '(1 2 3)
;;        ((_ _ _) 'all-thing) ; don't care about all the element
;;        (_ 'other-thing) ; we can mix sytle, too
;;        ((_) 'other-more-thing) ; or wrap it up to make it looks uniformly
;;        (otherwise 'last-thing)) ; "default" from, just for a fallback case
;; you can use `using' as well. It'll match each elment of "my-list" to, say, '(1 2 3), from left to
;; right. The element from "my-list" will be the 1st argument to the custom predicate function while
;; the element from '(1 2 3) will be the 2nd argument for the custom predicate function.
;; We can also use a variable instead of a list '(1 2 3) and the like as well. Ex.
;; (match my-list
;;       (my-other-list 'something))
;; Where `my-other-list' is a list.

;;; Notes:
;; `match' can cause side-effect.
;; `otherwise' doesn't need to be the final case, no other cases, below `otherwise' case,  will be
;; checked and run.
;; If custom predicate functionn isn't given, `match' will use `equal?'

;;; Code:

(define-library (beans patmat)
  (import (scheme base))
  (export match)

  (begin
    (define-syntax match
      (syntax-rules (otherwise using _)

        ;; cases of `otherwise'
        ((match val using comparator (otherwise body ...) another-body ...)
         (begin
           body ...))
        ((match val (otherwise body ...) another-body ...)
         (match val using equal? (otherwise body ...)))
        ((match (otherwise body ...) another-body ...)
         (begin
           body ...))


        ;; cases when there is no body
        ((match val using comparator)
         (comparator val val))
        ((match val)
         (match val using equal?))


        ;;; match a list
        ;; we recursively match until the end of the list, which means this list matches the patter.
        ((match val using comparator (() pat-body ...) body ...)
         (begin
           pat-body ...))
        ((match val using comparator ((_ pat-head-2 ...) pat-body ...) body ...)
         (match (cdr val) using comparator ((pat-head-2 ...) pat-body ...) body ...))
        ((match val using comparator ((pat-head pat-head-2 ...) pat-body ...) body ...)
         (if (comparator (car val) pat-head)
             (match (cdr val) using comparator ((pat-head-2 ...) pat-body ...) body ...)
             (match val using comparator
                    body ...)))


        ;;; match a single value
        ((match val using comparator (_ pat-body ...) body ...)
         (begin
           pat-body ...))
        ((match val using comparator (pat-head pat-body ...) body ...)
         (if (or (comparator val (quote pat-head))
                 (comparator val pat-head))
             (begin
               pat-body ...)
             (match val using comparator
                    body ...)))

        ;; case when not supply the custom comparator function
        ((match val (pat-head pat-body ...) body ...)
         (match val using equal? (pat-head pat-body ...) body ...))))))


;;;; patmat.scm ends here.
