(test-begin "patmat")

(load "patmat.scm")

(define *x* 1)
(define *y* 'test-symbol)

;; 1 expression of `otherwise'
(test-equal 2
            (match *x*
                   (otherwise 2)))
(test-equal 2
            (match *y*
                   (otherwise 2)))
;; 2 expression of `otherwise'
(test-equal 3
            (match *x*
                   (otherwise 2 3)))
(test-equal 3
            (match *y*
                   (otherwise 2 3)))
;; 3 expression of `otherwise'
(test-equal 4
            (match *x*
                   (otherwise 2 3 4)))
(test-equal 4
            (match *y*
                   (otherwise 2 3 4)))
;; test that `otherwise' is the final case of `match', it will not test other cases after `otherwise'
(test-equal 2
            (match *x*
                   (otherwise 2)
                   (1 3)))
(test-equal 'ok
            (match *y*
                   (otherwise 'ok)
                   (1 3)))

;; test whether it can manipulate external state
;; Answer: it can.
(test-equal 2
            (begin
              (match *x*
                     (otherwise 2 (set! *x* 2) 4))
              *x*))
;; reset the test value back
(set! *x* 1)

;; ending case without `otherwise'
(test-equal #true
            (match *x*))
;; `using' command
(test-equal #false
            (match *x* using (lambda (x y) #false)))

;; matching with not-a-list case
(test-equal 2
            (match *x*
                   (1 2)))
(test-equal 2
            (match *y*
                   (test-symbol 2)))
(test-equal 2
            (match *x*
                   (1 2)
                   (otherwise 3)))
(test-equal 2
            (match *y*
                   (test-symbol 2)
                   (otherwise 3)))
(test-equal 3
            (match (+ *x* 1)
                   (1 2)
                   (otherwise 3)))
;; matching with not-a-list case and `_' identifier
(test-equal 2
            (match *x*
                   (_ 2)))
(test-equal 2
            (match *y*
                   (_ 2)))
(test-equal 2
            (match *x*
                   (_ 2)
                   (otherwise 3)))
(test-equal 2
            (match *y*
                   (_ 2)
                   (otherwise 3)))
(test-equal 2
            (match (+ *x* 1)
                   (_ 2)
                   (otherwise 3)))
;; matching with not-a-list case; testing ordering
(test-equal 2
            (match *x*
                   (3 4)
                   (1 2)))
(test-equal 2
            (match *y*
                   (3 4)
                   (test-symbol 2)))
(test-equal 2
            (match *x*
                   (3 4)
                   (1 2)
                   (otherwise 3)))
(test-equal 2
            (match *y*
                   (3 4)
                   (test-symbol 2)
                   (otherwise 3)))
(test-equal 3
            (match (+ *x* 1)
                   (3 4)
                   (1 2)
                   (otherwise 3)))
;; matching with not-a-list case; testing ordering with `_'
(test-equal 4
            (match *x*
                   (_ 4)
                   (1 2)))
(test-equal 4
            (match *y*
                   (_ 4)
                   (1 2)))
(test-equal 4
            (match *x*
                   (_ 4)
                   (1 2)
                   (otherwise 3)))
(test-equal 4
            (match *y*
                   (_ 4)
                   (1 2)
                   (otherwise 3)))
(test-equal 4
            (match (+ *x* 1)
                   (1 2)
                   (_ 4)
                   (otherwise 3)))

;;;;;;;;;;;;;;;;;
;;; List case ;;;
;;;;;;;;;;;;;;;;;

(define *list* '(1 2 3))

;; it shouldn't matter what is after the `match' keyword since this macro will look at the
;; "case" to compare whether it's of the form of a single value or a list
(test-equal 9
            (match *list*
                   (_ 9)))

;; different places of `_'
(test-equal 9
            (match *list*
                   ((_ 2 3) 9)))
(test-equal 9
            (match '(1 4 3)
                   ((1 _ 3) 9)))
(test-equal 9
            (match '(1 2 4)
                   ((1 2 _) 9)))

;; empty case is equal to true
(test-equal 9
            (match *list*
                   (() 9)))

;; it's all YES's
(test-equal 9
            (match *list*
                   ((_ _ _) 9)))

;; the value we match should equal to itself
(test-equal 9
            (match *list*
                   (*list* 9)))

;; some ordering tests
(test-equal 9
            (match *list*
                   ((2 3 4) 8)
                   ((1 2 3) 9)))
(test-equal 10
            (match *list*
                   ((2 3 4) 8)
                   (otherwise 10)
                   (*list* 9)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Custome `comparator' function ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; just a simple non-standard comparator function
(define (my-comparator x y)
  (<= x y))

(define *my-val* 1)
(define *my-list* '(1 2 3))

(test-equal 9
            (match *my-val* using my-comparator
                   (0 8)
                   (1 9)
                   (otherwise 100)))
(test-equal 9
            (match *my-val* using my-comparator
                   (_ 9)
                   (1 8)
                   (otherwise 100)))
(test-equal 9
            (match *my-val* using my-comparator
                   (1 9)
                   (1 8)
                   (otherwise 100)))
(test-equal 9
            (match *my-val* using my-comparator
                   (_ 9)))
(test-equal 9
            (match *my-val* using my-comparator
                   (otherwise 9)))


(test-equal 9
            (match *my-list* using my-comparator
                   ((_ 5 6) 9)))
(test-equal 9
            (match *my-list* using my-comparator
                   ((2 _ 6) 9)))
(test-equal 9
            (match *my-list* using my-comparator
                   ((2 4 _) 9)))

(test-equal 9
            (match *my-list* using my-comparator
                   (() 9)))

(test-equal 9
            (match *my-list* using my-comparator
                   ((_ _ _) 9)))
(test-equal 9
            (match *list* using my-comparator
                   ((0 1 2) 8) ; simulate failed match
                   ((1 2 3) 9)))
(test-equal 10
            (match *list* using my-comparator
                   ((0 0 0) 8) ; simulate failed match
                   (otherwise 10)
                   (*list* 9)))


(test-end "patmat")
