;;;; stats.scm --- Statistical functions and methods -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:


;;; Code:

(define-library (beans math stats)
  (export z-table mean median cumsum variance standard-deviation
          correlation z-score t-score t-table
          geometric-mean root-mean-square harmonic-mean
          kurtosis skewness covariance spearman-correlation)
  (import (scheme base)
          (scheme inexact)
          (beans list-utils)
          (beans sort)
          (beans math))
  (begin

    ;;; basic formulas

    (define (mean numbers)
      (/ (apply + numbers) (length numbers)))

    (define (geometric-mean numbers)
      (let ((len (length numbers)))
        (expt (apply * numbers) (/ len))))

    (define (root-mean-square numbers)
      (let ((len (length numbers)))
        (sqrt (/ (apply + (map square numbers))
                 len))))

    (define (harmonic-mean numbers)
      (let ((len (length numbers)))
        (/ len (apply + (map (lambda (x) (/ x)) numbers)))))

    (define (median nums)
      (let ((numbers (apply sort nums)))
        (if (odd? (length numbers))
            (list-ref numbers (- (round (/ (length numbers) 2)) 1))
            (let* ((before (- (round (/ (length numbers) 2)) 1))
                   (after (+ before 1)))
              (mean (list (list-ref numbers before) (list-ref numbers after)))))))

    ;; cummulate sum of numbers
    (define (cumsum nums)
      (let loop ((result '())
                 (input nums))
        (cond
         ((null? input)
          (reverse result))
         ((null? result)
          (loop (cons (car input) result)
                (cdr input)))
         (else
          (let ((next (+ (car result)
                         (car input))))
            (loop (cons next result)
                  (cdr input)))))))

    (define (variance nums)
      (let ((mean-nums (mean nums)))
        (/ (apply + (map (lambda (x) (square (- x mean-nums))) nums))
           (- (length nums) 1))))

    (define (standard-deviation nums)
      (sqrt (variance nums)))

    (define (kurtosis nums)
      (let ((mean-nums (mean nums))
            (n (length nums)))
        (- (/ (/ (apply + (map (lambda (x) (expt (- x mean-nums) 4)) nums))
                 n)
              (square (/ (apply + (map (lambda (x) (square (- x mean-nums))) nums))
                         n)))
           3)))

    ;; this is an estimator of the population skewness but we will not use this vauel. We will use
    ;; it to calculate adjusted Fisher–Pearson standardized moment coefficient, which is a more
    ;; popular definition of sample skewness
    (define (%skewness nums)
      (let ((mean-nums (mean nums))
            (n (length nums)))
        (/ (/ (apply + (map (lambda (x) (expt (- x mean-nums) 3)) nums)) n)
           (expt (/ (apply + (map (lambda (x) (square (- x mean-nums))) nums)) (- n 1)) (/ 3 2)))))

    ;; a sample skewness.
    ;; `b1' here is the symbol [[https://en.wikipedia.org/wiki/Skewness][Wikipedia]] use so let's
    ;; use it too for consistency.
    (define (skewness nums)
      (let ((b1 (%skewness nums))
            (n (length nums)))
        (/ (* (square n) b1)
           (* (- n 1) (- n 2)))))

    ;; Finding a linear correlation between 2 sets of data.
    ;; This is Pearson correlation coefficient.
    ;; Data for testing can be found in https://en.wikipedia.org/wiki/Simple_linear_regression
    (define (correlation x-list y-list)
      (if (not (= (length x-list)
                  (length y-list)))
          (error "Two data sets don't have the same lengeth" correlation)
          (let ((x-mean (mean x-list))
                (x-sd (standard-deviation x-list))
                (y-mean (mean y-list))
                (y-sd (standard-deviation y-list)))
            (* (/ (- (length x-list) 1))
               (apply + (map (lambda (x y)
                               (* (/ (- x x-mean)
                                     x-sd)
                                  (/ (- y y-mean)
                                     y-sd)))
                             x-list
                             y-list))))))

    ;; A sample covariance.
    (define (covariance x-list y-list)
      (unless (= (length x-list)
                 (length y-list))
        (error "Two data sets don't have the same lengeth" covariance))
      (let ((mean-x (mean x-list))
            (mean-y (mean y-list)))
        (/ (apply + (map (lambda (x y)
                           (* (- x mean-x)
                              (- y mean-y)))
                         x-list
                         y-list))
           (- (length x-list) 1))))

    (define (spearman-correlation x-list y-list)
      (unless (= (length x-list)
                 (length y-list))
        (error "Two data sets don't have the same lengeth" spearman-correlation))
      (let* ((ranked-x (quicksort x-list))
             (ranked-y (quicksort y-list))
             (cor (covariance ranked-x ranked-y))
             (sd-x (standard-deviation ranked-x))
             (sd-y (standard-deviation ranked-y)))
        (/ cor (* sd-x sd-y))))

    ;;; Z statistics
    ;; We have to split the integral to smaller size to get more accurate result
    (define (%z-table z1 z2)
      (integral (lambda (x)
                  (* (/ (sqrt (* 2 +PI+)))
                     (/ (exp (/ (square x) 2)))))
                z1
                z2))

    ;; It'n one-side of the normal distribution, from 0 to z.
    ;; We recussively add z-table of smaller regions together to get more accuratet result.
    ;; The step size of 0.1 is arbitary but it gives good enought accuracy to around 6 decimal places.
    (define (z-table z)
      (if (<= z 0.1)
          (%z-table 0 z)
          (+ (%z-table (- z 0.1) z)
             (z-table (- z 0.1)))))

    (define (z-score x mean-value sd-value)
      (/ (- x mean-value)
         sd-value))

    (define (%t-statistics--gamma-term v)
      (cond
       ((and (odd? v)
             (> v 1))
        (* (/ +PI+)
           (/ (sqrt v))
           (do ((i v (- i 2))
                (result 1 (* result (/ (- i 1) (- i 2)))))
               ((= i 1) result))))
       ((and (even? v)
             (> v 1))
        (* (/ 2)
           (/ (sqrt v))
           (do ((i v (- i 2))
                (result 1 (* result (/ (- i 1) (- i 2)))))
               ((= i 2) result))))
       (else
        (error (string-append (number->string v) " is invalide degree of freedom")
               %t-statistics--gamma-term))))

    (define (%t-statistics--pdf t v)
      (* (%t-statistics--gamma-term v)
         (expt (+ 1 (/ (square t) v))
               (- (/ (+ v 1) 2)))))

    (define (%t-table t1 t2 v)
      (integral (lambda (x)
                  (%t-statistics--pdf x v))
                t1
                t2))

    (define (t-score sample-mean population-mean sample-sd sample-size)
      (/ (- sample-mean population-mean)
         (/ sample-sd
            (sqrt sample-size))))

    ;; `t' is the value of t-score we want and `v' is degree of freedom.
    ;; That is, it must be use like (t-table (t-score sample-mean population-mean sample-sd sample-size) v)
    (define (t-table t v)
      (if (<= t 0.1)
          (%t-table 0 t v)
          (+ (%t-table (- t 0.1) t v)
             (t-table (- t 0.1) v))))))


;;;; stats.scm ends here.
