;;;; math.scm --- mathmatical utilities -*- mode: Scheme -*-

;;; Beans Project
;;; Copyright (C) 2022, 2023 generoberts
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Commonly used mathematical and statistical functinos.

;;; Code:

(define-library (beans math)
  (export +PI+ +E+
          prime? *iwi-step-size*
          derivative integral integral-wide-interval root-finding diff-eq-solver partial-derivative)
  (import (scheme base)
          (scheme inexact))
  (begin

    (define +PI+ 3.141592653589793)

    (define +E+ 2.7182818284590452)


    (define +small-primes+
      (list 2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89
            97 101 103 107 109 113 127 131 137 139 149 151 157 163 167 173 179
            181 191 193 197 199 211 223 227 229 233 239 241 251 257 263 269 271
            277 281 283 293 307 311 313 317 331 337 347 349 353 359 367 373 379
            383 389 397 401 409 419 421 431 433 439 443 449 457 461 463 467 479
            487 491 499 503 509 521 523 541 547 557 563 569 571 577 587 593 599
            601 607 613 617 619 631 641 643 647 653 659 661 673 677 683 691 701
            709 719 727 733 739 743 751 757 761 769 773 787 797 809 811 821 823
            827 829 839 853 857 859 863 877 881 883 887 907 911 919 929 937 941
            947 953 967 971 977 983 991 997 1009))

    (define +last-small-primes+ (car (reverse +small-primes+)))

    ;; Try to divide `num' with small primes to speed up the process.
    ;; Returns `#true' if some small prime can divide `num'. Returns `#false' otherwise.
    (define (%try-with-small-primes num)
      (let loop ((num num)
                 (primes (cdr +small-primes+)) ; skip 2
                 (found? #false))
        (cond
         ((null? primes)
          #false)
         (found?
          #true)
         (else
          (let ((divided (/ num (car primes))))
            (if (integer? divided)
                (loop num primes #true)
                (loop num (cdr primes) #false)))))))

    ;; using the good old method to find prime
    (define (%prime? start num)
      (if (even? num)
          #false
          (let ((divided (/ num start)))
            (cond
             ((and (integer? divided)
                   (not (= start num)))
              #false)
             ;; since it's a helper function for `prime?' we don't start from 1. Thus when START and
             ;; NUM are the same, we know that only 1 and START can divide NUM so it's prime.
             ((= start num)
              #true)
             (else
              (%prime? (+ start 1) num))))))

    ;; checking prime numbers
    (define (prime? num)
      (cond
       ((not (number? num))
        (error "Not a number." prime?))
       ((or (negative? num)
            (not (integer? num)))
        (error "Prime numbers must be positive integer" prime?))
       ((<= num +last-small-primes+)
        (if (member num +small-primes+)
            #true
            #false))
       ((%try-with-small-primes num)
        #false)
       (else
        (%prime? +last-small-primes+ num))))

    ;; To calculate differential numerically, we use this constant as a limit.
    (define *dx* 0.00001)

    ;; directly from a definition of derivative
    (define (derivative f)
      (lambda (x) (/ (- (f (+ x *dx*)) (f x))
                *dx*)))

    ;; partial derivative with respect to the ith argument
    ;; f is a function that take a fix number of arguments, that is it's of the form
    ;; (lambda (x y) ...) and not (lambda z ...)
    ;; This procedure returns a procedure that accept the same number of arguments as f.
    ;; Usage example: ((partial-derivative (lambda (x y) (+ x y)) 0) 1 1)
    (define (partial-derivative f i)
      (lambda args
        (let* ((args-copy (list-copy args))
               (arg (list-ref args-copy i)))
          (set! (list-ref args-copy i) (+ arg *dx*))
          (/ (- (apply f args-copy) (apply f args))
             *dx*))))

    ;; use Simpson's 3/8 rule to evaluate integration between point a and point b.
    (define (integral f a b)
      (* (/ (- b a) 8)
         (+ (f a)
            (* 3 (f (/ (+ (* 2 a) b)
                       3)))
            (* 3 (f (/ (+ a (* 2 b))
                       3)))
            (f b))))

    (define *iwi-step-size* 0.001)
    (define (integral-wide-interval f a b)
      (unless (<= a b)
        (error "invalide range" integral-wide-interval))
      (let loop ((result 0)
                 (start a)
                 (end b))
        (if (<= end start)
            result
            (loop (+ (integral f start (+ start *iwi-step-size*)) result)
                  (+ start *iwi-step-size*)
                  end))))

    ;; find a root, a point that make the given function `f' return 0, by
    ;; Newton's method.
    (define (root-finding f)

      (define (good-enough? target guess)
        (< (abs (- target guess))
           *dx*))

      (define (next f x)
        (- x (/ (f x) ((derivative f) x))))

      (define (newton-method f guess)
        (let ((next-value (next f guess)))
          (if (good-enough? guess next-value)
              next-value
              (newton-method f next-value))))

      (newton-method f 1))

    ;;; Runge-Kutta method.
    ;; Given a function `func', an initial values `state', and a `step-size',
    ;; and returns the next state as a list of next-t and next-y.
    ;; In this implementation, `func' is a function of two variables and `state' is a list of `t' and `y'.
    (define (rk4 func state step-size)
      (let* ((t (car state))
             (y (cadr state))
             (k1 (func t y))
             (k2 (func (+ t (/ step-size 2))
                       (+ y (* step-size (/ k1 2)))))
             (k3 (func (+ t (/ step-size 2))
                       (+ y (* step-size (/ k2 2)))))
             (k4 (func (+ t step-size)
                       (+ y (* step-size k3)))))
        (list (+ t step-size)
              (+ y (/ (* step-size (+ k1
                                      (* 2 k2)
                                      (* 2 k3)
                                      k4))
                      6)))))

    ;; Solving a differential equation using Runge-Kutta method.
    ;; Take argmuntes just like `rk4', but it takes a number of `times' it should product the result.
    (define (diff-eq-solver func init-state step-size times)

      (define (%diff-eq-solver func init-state step-size times result)
        (cond
         ((zero? times)
          (reverse result))
         ((null? result)
          (let ((first-result (rk4 func init-state step-size)))
            (%diff-eq-solver func
                             first-result
                             step-size
                             (- times 1)
                             (cons first-result (cons init-state result)))))
         (else
          (let* ((previous-result (car result))
                 (this-result (rk4 func previous-result step-size)))
            (%diff-eq-solver func
                             this-result
                             step-size
                             (- times 1)
                             (cons this-result result))))))

      (%diff-eq-solver func init-state step-size times '()))))

;;;; math.scm ends here.
